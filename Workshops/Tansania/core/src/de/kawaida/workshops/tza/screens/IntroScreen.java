package de.kawaida.workshops.tza.screens;

import java.util.HashMap;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTile.BlendMode;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

import de.kawaida.workshops.tza.WorkshopGameTZA;
import de.kawaida.workshops.tza.characters.Collectable;
import de.kawaida.workshops.tza.characters.Gem;
import de.kawaida.workshops.tza.characters.Enemy;
import de.kawaida.workshops.tza.characters.Player;
import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.config.InputConfig;
import de.kawaida.workshops.tza.config.Types;
import de.kawaida.workshops.tza.controllers.CollisionControl;
import de.kawaida.workshops.tza.entity.EntityUserData;
import de.kawaida.workshops.tza.util.ScreenArea;

public class IntroScreen extends Screen {
	
	private WorkshopGameTZA game;
	private OrthographicCamera cam;
	
	private CollisionControl ch;
	
	Body body;
	BodyDef bodyDef;
	
	PolygonShape polyShape;
	CircleShape circleShape;
	ChainShape chainShape;
	
	FixtureDef fixtureDef;
	FixtureDef polyFixture;
	FixtureDef circleFixture;

	private TmxMapLoader tmxLoader;
	private TiledMap tileMap;
	private TiledMapRenderer tmr;
	
	private Player player;
	private Collectable collectable;
	
	private double spawnCollectable = 0f;
	public static boolean gameOver = false;
//	public static boolean gameOver = true;
	
	private Vector2 scrCenter;
	private ScreenArea yesBtnArea;
	private ScreenArea noBtnArea;
	
	public boolean[] dropAreas;
	public HashMap<Body, Collectable> collectables;
	
	private int collectableCount = 0;
	
	public IntroScreen(WorkshopGameTZA game) {
		this.game = game;
		cam = game.cam;
		ch = game.ch;
		this.player = game.player;
	}

	@Override
	public void init() {
		tmxLoader = new TmxMapLoader();
		tileMap = tmxLoader.load("levels/level1.tmx");
		tmr = new OrthoCachedTiledMapRenderer(tileMap);

		bodyDef = new BodyDef();
		fixtureDef = new FixtureDef();
		TiledMapTileLayer layer = (TiledMapTileLayer) tileMap.getLayers().get("ground");
		
		int xSize = layer.getWidth(), ySize = layer.getHeight();
		Vector2[] shapeVectors = new Vector2[2];
		polyShape = new PolygonShape();
		
		shapeVectors[0] = new Vector2();
		shapeVectors[1] = new Vector2();
		bodyDef.type = BodyType.StaticBody;
		for (int i = 0; i < xSize; i++) {
			for (int j = 0; j < ySize; j++) {
				Cell cell = layer.getCell(i, j);
				if (cell == null) continue;
				if (cell.getTile() == null) continue;
				
				TiledMapTile tile = cell.getTile();
				
				int tID = tile.getId();
				switch (tID) {
				case 1:
					createSquareBody(i, j, 2);
					break;
				case 2:
					createSquareBody(i, j, 2);
					break;
				case 3:
					createSquareBody(i, j, 2);
					break;
				case 4:
					createAngleBody(i, j, 1);
					break;
				case 5:
					createAngleBody(i, j, 0);
					break;
				case 6:
					createAngleBody(i, j, 1);
					break;
				case 7:
					createAngleBody(i, j, 1);
					break;
				case 8:
					createAngleBody(i, j, 0);
					break;
				case 9:
					createAngleBody(i, j, 0);
					break;
				}
			}
		}
		
		scrCenter = new Vector2(GameCfg.RES).scl(.5f);

		Vector2 pos = new Vector2(scrCenter);
		pos.sub(32, 32);
		pos.sub(100, 0);
		
		yesBtnArea = new ScreenArea(pos, new Vector2(64, 64));
		yesBtnArea.setTexture(new Texture("sprites/interface/yesBtn.png"));
		
		pos = new Vector2(scrCenter);
		pos.sub(32, 32);
		pos.add(100, 0);
		
		noBtnArea = new ScreenArea(pos, new Vector2(64, 64));
		noBtnArea.setTexture(new Texture("sprites/interface/noBtn.png"));
		
		dropAreas = new boolean[15];
		collectables = new HashMap<Body, Collectable>();
	}
	
	private void createAngleBody(int x, int y, int tID) {
		float tileSize = .32f;
		Vector2[] shapeVectors = new Vector2[2];
		
		bodyDef.position.set((x + .5f) * tileSize * GameCfg.SCALE.x, (y + .5f) * tileSize * GameCfg.SCALE.y);
		
		chainShape = new ChainShape();
		if (tID == 0)  {
			shapeVectors[0] = new Vector2(-tileSize * GameCfg.SCALE.x / 2f, -tileSize * GameCfg.SCALE.y / 2f);
			shapeVectors[1] = new Vector2(tileSize * GameCfg.SCALE.x / 2f, tileSize * GameCfg.SCALE.y / 2f);
		} else {
			shapeVectors[0] = new Vector2(-tileSize * GameCfg.SCALE.x / 2f, tileSize * GameCfg.SCALE.y / 2f);
			shapeVectors[1] = new Vector2(tileSize * GameCfg.SCALE.x / 2f, -tileSize * GameCfg.SCALE.y / 2f);
		}

		chainShape.createChain(shapeVectors);
		
		fixtureDef.shape = chainShape;
		fixtureDef.friction = .0f;
		fixtureDef.filter.categoryBits = Types.SOLID;
		fixtureDef.filter.maskBits = Types.PLAYER | Types.COLLECTABLE;
		game.world.createBody(bodyDef).createFixture(fixtureDef).setUserData(new EntityUserData("ground-" + x + "-" + y));
	}
	
	private void createSquareBody(int x, int y, int tID) {
		float tileSize = .32f;
		Vector2[] shapeVectors = new Vector2[2];
		
		bodyDef.position.set((x + .5f) * tileSize * GameCfg.SCALE.x, (y + .5f) * tileSize * GameCfg.SCALE.y);
		
		chainShape = new ChainShape();
		shapeVectors[0] = new Vector2(-tileSize * GameCfg.SCALE.x / 2f, tileSize * GameCfg.SCALE.y / 2f);
		shapeVectors[1] = new Vector2(tileSize * GameCfg.SCALE.x / 2f, tileSize * GameCfg.SCALE.y / 2f);

		chainShape.createChain(shapeVectors);
		
		fixtureDef.shape = chainShape;
		fixtureDef.friction = .0f;
		fixtureDef.filter.categoryBits = Types.SOLID;
		fixtureDef.filter.maskBits = Types.PLAYER | Types.COLLECTABLE;
		game.world.createBody(bodyDef).createFixture(fixtureDef).setUserData(new EntityUserData("ground-" + x + "-" + y));
	}

	@Override
	public void update(float t) {
		int dropArea;
		
		if (!gameOver) {
			dropArea = (int) (Math.random() * 14f);
			
			spawnCollectable = Math.random();
			player.update(t);
			
			if (spawnCollectable > .98f && !dropAreas[dropArea]) {
				collectable = new Gem(game.world);
				collectable.getBody().setTransform(new Vector2((dropArea + 1) * .64f, GameCfg.RES.y / 100f + .5f), 0);
				collectable.dropArea = dropArea;
				dropAreas[dropArea] = true;
				collectables.put(collectable.getBody(), collectable);
			}

			spawnCollectable = Math.random();
			if (spawnCollectable > .992f && !dropAreas[dropArea]) {
				collectable = new Enemy(game.world);
				collectable.getBody().setTransform(new Vector2((dropArea + 1)  * .64f, GameCfg.RES.y / 100f + .5f), 0);
				collectable.dropArea = dropArea;
				dropAreas[dropArea] = true;
				collectables.put(collectable.getBody(), collectable);
			}
		}
		handleInput();
		
		Array<Body> a = new Array<Body>();
		game.world.getBodies(a);
		
		for (Body b : a) {
			if (b.getPosition().y < 0) {
				WorkshopGameTZA.bodiesToRemove.add(b);
			}
		}
		
		for (Body b : WorkshopGameTZA.bodiesToRemove) {
			dropAreas[collectables.get(b).dropArea] = false;
			game.world.destroyBody(b);
			collectables.remove(b);
		}
		WorkshopGameTZA.bodiesToRemove.clear();
	}

	@Override
	public void render() {
		tmr.setView(cam);
		tmr.render();
		
		WorkshopGameTZA.batch.begin();
		WorkshopGameTZA.batch.draw(player.tex, player.getDrawPosition().x, player.getDrawPosition().y);
		
		if (gameOver) {
			WorkshopGameTZA.batch.draw(yesBtnArea.getTexture(),  yesBtnArea.position.x, yesBtnArea.position.y);
			WorkshopGameTZA.batch.draw(noBtnArea.getTexture(),  noBtnArea.position.x, noBtnArea.position.y);
		}
		
		for (Collectable c : collectables.values()) {
			Vector2 v = c.getDrawPosition();
			WorkshopGameTZA.batch.draw(c.getTex(),v.x, v.y);
		}
		WorkshopGameTZA.batch.end();
	}
	
	@Override
	public void debugRender() {
		
	}

	@Override
	public void handleInput() {
		handleDesktop();
		if (InputConfig.isPressed(InputConfig.TOUCH)) {
			System.out.println("touch " + InputConfig.touchPos);
			handleTouch();
		}
	}
	
	private void handleDesktop() {
		if (!gameOver) {
			body = player.getBody();
			
			if (InputConfig.isPressed(InputConfig.JUMP_KEY) && ch.isPlayerOnGround()) {
				body.applyForceToCenter(body.getLinearVelocity().x, 200f, false);
			}
			
			if (InputConfig.isPressed(InputConfig.MOVE_LEFT_KEY)) {
				body.setLinearVelocity(-player.getMoveSpeed(), body.getLinearVelocity().y);
//				player.setFaceDirection(Direction.LEFT);
			}
			
			if (InputConfig.isPressed(InputConfig.MOVE_RIGHT_KEY)) {
				body.setLinearVelocity(player.getMoveSpeed(), body.getLinearVelocity().y);
//				player.setFaceDirection(Direction.RIGHT);
			}
			
			if (InputConfig.isReleased(InputConfig.MOVE_LEFT_KEY)) {
				body.setLinearVelocity(0f, body.getLinearVelocity().y);
			}
			
			if (InputConfig.isReleased(InputConfig.MOVE_RIGHT_KEY)) {
				body.setLinearVelocity(0f, body.getLinearVelocity().y);
			}
			
			if (InputConfig.isDown(InputConfig.MOVE_LEFT_KEY)) {
				body.setLinearVelocity(-player.getMoveSpeed(), body.getLinearVelocity().y);
			}
			
			if (InputConfig.isDown(InputConfig.MOVE_RIGHT_KEY)) {
				body.setLinearVelocity(player.getMoveSpeed(), body.getLinearVelocity().y);
			}
		} else {
			if (InputConfig.isPressed(InputConfig.ACKNOWLEDGE)) {
				restart();
			}
			
			if (InputConfig.isPressed(InputConfig.DECLINE)) {
				game.dispose();
				Gdx.app.exit();
			}
		}
	}
	
	private void handleTouch() {

		if (!gameOver)	{
			player.moveTo(InputConfig.touchPos);
			
//			if (InputConfig.touchPos.x < w / 2f) {
//				body.setLinearVelocity(-player.getMoveSpeed(), body.getLinearVelocity().y);
//			}
//			
//			if (InputConfig.touchPos.x > w / 2f) {
//				body.setLinearVelocity(player.getMoveSpeed(), body.getLinearVelocity().y);
//			}
		} else {
			if (yesBtnArea.contains(InputConfig.touchPos)) {
				restart();
				return;
			}
			
			if (noBtnArea.contains(InputConfig.touchPos)) {
				game.dispose();
				Gdx.app.exit();
			}
		}
	}
	
	public void restart() {
		gameOver = false;
		player.life = 3;
		player.score = 0;
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void dispose() {
		tileMap.dispose();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}
}