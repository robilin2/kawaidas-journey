package de.kawaida.workshops.tza.screens;

public class ScreenManager {
	
	private static Screen currentScreen;
	
	public static void setScreen(Screen screen) {
		if (currentScreen != null)
			currentScreen.dispose();
		
		currentScreen = screen;
		currentScreen.init();
	}
	
	public static Screen getScreen() {
		return currentScreen;
	}
	
	public static void update(float t) {
		currentScreen.update(t);
	}
}