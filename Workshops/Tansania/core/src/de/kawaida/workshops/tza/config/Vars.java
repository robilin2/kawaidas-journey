package de.kawaida.workshops.tza.config;

public class Vars {
	
	public enum Status { PLAYER_IDLE, PLAYER_WALK };
	
	public static class Directions {
		public static final short LEFT = 0x1;
		public static final short RIGHT = 0x2;
		public static final short DOWN = 0x4;
		public static final short UP = 0x8;
	}
	
	public enum Direction { LEFT, RIGHT, DOWN, UP };
	
}