package de.kawaida.kjgame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.venjinx.core.libgdx.VJXGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration lwjglCfg = new LwjglApplicationConfiguration();

        lwjglCfg.title = "Kawaida - Game Workshop Tansania";
        lwjglCfg.width = 1280;
        lwjglCfg.height = 720;
        lwjglCfg.foregroundFPS = 60;

        if (arg == null || arg.length == 0) {
            arg = new String[1];
            arg[0] =
                                                        "D:/Development/Projekte/Kawaida/Dev/kawaidas-journey/"
                                                        + "KawaidasJourney/android/assets/"
                                            + "development/level/dev_lvl_00_test_level.tmx";
        }
        //        arg[0] = "development/level/" + "dev_lvl_00_test_level.tmx";

        // Start Game
        new LwjglApplication(new VJXGame(arg), lwjglCfg);
        //        new LwjglApplication(new ParaTest(), lwjglCfg);
    }
}