package de.kawaida.kjgame.actions;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

import de.venjinx.core.libgdx.entity.Entity;

public class MoveAction extends TemporalAction {

    private Vector2 direction = new Vector2();
    private float speed = 1f;

    public MoveAction(Vector2 direction, float speed) {
        super();
        this.direction = direction;
        this.speed = speed;
    }

    public void setDirection(Vector2 dir) {
        direction = dir;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    protected void update(float percent) {
        ((Entity) target).getBody().setLinearVelocity(direction.scl(speed));
    }

}