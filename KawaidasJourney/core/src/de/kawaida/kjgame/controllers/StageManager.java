package de.kawaida.kjgame.controllers;

import java.util.HashMap;

import de.venjinx.core.libgdx.VJXStage;

public class StageManager {

    private static HashMap<String, VJXStage> stages = new HashMap<String, VJXStage>();

    public static void addStage(VJXStage stage, String name) {
        stages.put(name, stage);
        //        InputControl.addStage(stage);
    }

    public static <T> T getStage(String name, Class<T> type) {

        @SuppressWarnings("unchecked")
        T obj = (T) stages.get(name);

        return obj;
    }

    public static void removeStage(VJXStage stage) {
        stage.dispose();
        stages.remove(stage);
    }

    public static void update(float delta) {
        for (VJXStage s : stages.values())
            s.act(delta);
    }

    public static void draw(String name) {
        stages.get(name).draw();
    }

    public static void drawAll() {
        for (VJXStage s : stages.values())
            s.draw();
    }
}