package de.kawaida.kjgame.controllers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.kawaida.kjgame.entities.actors.Kawaida;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.entity.NewEntity;

public class CollisionControl implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture f;
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        short cat0 = fa.getFilterData().categoryBits;
        short cat1 = fb.getFilterData().categoryBits;

        VJXData fData0 = (VJXData) fa.getUserData();
        VJXData fData1 = (VJXData) fb.getUserData();

        NewEntity e0 = fData0.getEntity();
        NewEntity e1 = fData1.getEntity();

        //        System.out.println(fData0.getName());
        //        System.out.println(fData1.getName());
        //        System.out.println("_--------------------");

        if (e1 instanceof Kawaida) {
            f = fa;
            fa = fb;
            fb = f;

            fData0 = (VJXData) fa.getUserData();
            fData1 = (VJXData) fb.getUserData();

            e0 = fData0.getEntity();
            e1 = fData1.getEntity();
        }

        if (!e0.isAlive() || !e1.isAlive()) return;

        int s = cat0 | cat1;
        if (isCategory((short) s, PhysCat.NO_PASS)) {
            if (fData0.getName().contains("feet")) {
                e1.setSensor(false);
                if (!e0.isOnGround()) {
                    if (e0.getBody().getLinearVelocity().x == 0) {
                        e0.setStatus(Status.IDLE);
                    } else {
                        if (e0.getStatus() != Status.RUN) {
                        }
                        e0.setStatus(Status.RUN);
                    }
                }
                e0.incrOnGround();
                return;
            }

            if (fData0.getName().contains("head")) {
                e1.setSensor(true);
                return;
            }
        }

        int s1 = PhysCat.PLAYER.id | PhysCat.TRIGGER.id;
        if ((s & s1) == s1) {
            e0.trigger(fb);
            return;
        }

        s1 = PhysCat.ENEMY.id | PhysCat.WEAPON.id;
        if ((s & s1) == s1) {
            if (isCategory(cat0, PhysCat.ENEMY)) {
                e0.setStatus(Status.DIE, true);

                e1.getBody().setLinearVelocity(0f, 2f);
            } else {
                e1.setStatus(Status.DIE, true);

                e0.getBody().setLinearVelocity(0f, 2f);
            }
            return;
        }

        s1 = PhysCat.PLAYER.id | PhysCat.ITEM.id;
        if ((s & s1) == s1) {
        }

        e0.collideWith(e1);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        VJXData fData0 = (VJXData) fa.getUserData();
        VJXData fData1 = (VJXData) fb.getUserData();

        NewEntity e0 = fData0.getEntity();
        NewEntity e1 = fData1.getEntity();

        short cat0 = e0.getB2DCategory();
        short cat1 = e1.getB2DCategory();

        int s = cat0 | cat1;
        if (isCategory((short) s, PhysCat.NO_PASS)) {
            if (isCategory(cat0, PhysCat.NO_PASS)) e0 = e1;

            e0.decrOnGround();
            return;
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private boolean isCategory(short cat, PhysCat physCat) {
        return (cat & physCat.id) > 0;
    }
}