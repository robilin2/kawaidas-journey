package de.kawaida.kjgame.stages;

import java.util.ArrayDeque;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import de.kawaida.kjgame.entities.actors.Kawaida;
import de.kawaida.ui.IngameMenu;
import de.kawaida.ui.LoseMenu;
import de.kawaida.ui.PauseMenu;
import de.kawaida.ui.ProjectsPopupMenu;
import de.kawaida.ui.SettingsMenu;
import de.kawaida.ui.ShopMenu;
import de.kawaida.ui.ShopPrimeMenu;
import de.kawaida.ui.StoryDialog;
import de.kawaida.ui.UiTable;
import de.kawaida.ui.UserInterface;
import de.kawaida.ui.WinMenu;
import de.kawaida.ui.skins.InterfaceSkin;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;
import de.venjinx.core.libgdx.entity.Entity;

public class IngameHUD extends VJXStage implements EventListener {

    private Kawaida pAvatar;

    Actor mapActor = new Actor();
    Actor restart = new Actor();
    Actor saveMe = new Actor();
    Actor shop = new Actor();
    Actor giveUp = new Actor();
    Actor buyTotem = new Actor();

    private InterfaceSkin hudSkin;

    private UserInterface uHUD;
    private IngameMenu ingameMenu;

    private PauseMenu pauseMenu;
    private WinMenu winMenu;
    private LoseMenu loseMenu;
    private ProjectsPopupMenu projectsPopup;
    private SettingsMenu settingsMenu;
    private ShopMenu shopMenu;
    private ShopPrimeMenu shopPrimeMenu;

    private boolean jumpPressed = false;
    private float chargeTime = 0f;

    private Sprite objective;

    public IngameHUD(VJXGame game) {
        super(game);
        setName("ingameHUD");

        setViewport(game.getInterfaceView());

        ingameMenu = new IngameMenu(game.menuSkin);

        resources.put("dialog0", new AssetDescriptor<>(
                        "menu/dialog/dialog0.jpg",
                        Texture.class));

        resources.put("dialog1", new AssetDescriptor<>(
                        "menu/dialog/dialog1.jpg",
                        Texture.class));

        resources.put("dialog2", new AssetDescriptor<>(
                        "menu/dialog/dialog2.jpg",
                        Texture.class));

        resources.put("dialog3", new AssetDescriptor<>(
                        "menu/dialog/dialog3.jpg",
                        Texture.class));

        resources.put("menu_win", new AssetDescriptor<>(
                        "menu/menu_win.jpg",
                        Texture.class));

        resources.put("menu_lose", new AssetDescriptor<>(
                        "menu/menu_lose.jpg",
                        Texture.class));

        resources.put("menu_pause", new AssetDescriptor<>(
                        "menu/menu_pause.jpg",
                        Texture.class));

        resources.put("plant_your_palm", new AssetDescriptor<>(
                        "menu/plant_your_palm.png",
                        Texture.class));

        resources.put("menu_settings", new AssetDescriptor<>(
                        "menu/settings/menu_settings.jpg",
                        Texture.class));

        resources.put("menu_projects_popup", new AssetDescriptor<>(
                        "menu/projects/menu_projects_popup.jpg",
                        Texture.class));

        resources.put("menu_shop", new AssetDescriptor<>(
                        "menu/shop/menu_shop.jpg",
                        Texture.class));

        resources.put("menu_shop_prime", new AssetDescriptor<>(
                        "menu/shop/menu_shop_prime.jpg",
                        Texture.class));
    }

    public void pause(boolean pause) {
        if (pause) {
            game.getScreen().pause();
            ingameControl = false;
            game.assetMngr.getMusic(
                            game.getScreen().getLevel().getBgMusicName())
                            .setVolume(.3f);
        } else {
            ingameMenu.remove();
            addActor(uHUD);

            uHUD.getJoystick().addListener(this);
            addActor(uHUD.getJoystick());

            game.getScreen().resume();
            ingameControl = true;
            game.assetMngr.getMusic(
                            game.getScreen().getLevel().getBgMusicName())
                            .setVolume(1f);
        }
    }

    public void showDialog(Entity leftEntity, Entity rightEntity) {
        clear();
        uHUD.getJoystick().remove();

        StoryDialog sd = ingameMenu.getStoryDialog();

        ArrayDeque<SpriteDrawable> parts = new ArrayDeque<>();
        parts.add(game.assetMngr.createSpriteDrawable("dialog0"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog1"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog2"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog3"));

        sd.setDialogParts(parts);

        ingameMenu.prepare();
        ingameMenu.setBackground(sd.nextSprite());
        addActor(ingameMenu);

        game.getScreen().pause();
        ingameControl = false;
    }

    private boolean gameOver = false;

    public void outro() {
        gameOver = true;

        ingameMenu.remove();
        game.getScreen().resume();

        Vector2 bodyVel = new Vector2();
        bodyVel.x = game.getPlayer().getAvatar().getSpeed() / 2f;
        pAvatar.setDirection(VJXDirection.Right);
        pAvatar.setStatus(Status.WALK);
        pAvatar.getBody().setLinearVelocity(bodyVel);
        ingameControl = false;
    }

    public void showProject() {
        clear();
        uHUD.getJoystick().remove();

        projectsPopup.prepare();
        addActor(projectsPopup);

        game.getScreen().pause();
        ingameControl = false;

        last = null;
    }

    public void showPause() {
        pause(true);
        clear();
        uHUD.getJoystick().remove();

        pauseMenu.prepare();
        addActor(pauseMenu);

        game.sfx.stopAllSounds();

        ingameControl = false;
    }

    private boolean won = false;

    public void win() {
        clear();
        uHUD.getJoystick().remove();

        winMenu.prepare();
        addActor(winMenu);

        if (!won) {
            game.sfx.stopAllSounds();
            game.sfx.play("sfx_gl_stg_win_jingle", false);
            time = 0;
            c = 0;
        }

        //        game.getScreen().pause();

        ingameControl = false;
        won = true;
        last = winMenu;
    }

    public void lose() {
        clear();
        uHUD.getJoystick().remove();

        loseMenu.prepare();
        addActor(loseMenu);

        game.sfx.stopAllSounds();
        game.sfx.play("sfx_gl_stg_lose_jingle", false);

        game.getScreen().pause();

        ingameControl = false;
        last = loseMenu;
    }

    @Override
    protected void init() {
        ArrayList<TextureAtlas> tas = new ArrayList<>();
        tas.add(game.assetMngr.getSpriteSheet("controls"));
        tas.add(game.assetMngr.getSpriteSheet("gl_stage"));

        hudSkin = new InterfaceSkin(tas, game.font);

        uHUD = new UserInterface(hudSkin, "ui_main", game.getPlayer());
        addActor(uHUD);

        uHUD.getJoystick().addListener(this);
        addActor(uHUD.getJoystick());

        uHUD.update();

        pAvatar = game.getPlayer().getAvatar();

        giveUp.setName("ui_btn_giveUp");
        //        giveUp.setPosition(250, 50);
        //        giveUp.setSize(230, 75);

        restart.setName("ui_btn_restart");
        //        restart.setPosition(500, 5);
        //        restart.setSize(230, 75);

        mapActor.setName("ui_btn_map");
        //        mapActor.setPosition(250, 50);
        //        mapActor.setSize(230, 75);

        buyTotem.setName("ui_btn_buy");
        //        buyTotem.setPosition(400, 460);
        //        buyTotem.setSize(100, 75);

        shop.setName("ui_btn_shop");
        //        shop.setPosition(730, 460);
        //        shop.setSize(230, 75);

        pauseMenu = new PauseMenu(game.menuSkin);
        pauseMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_pause"));

        winMenu = new WinMenu(game.menuSkin);
        winMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_win"));

        loseMenu = new LoseMenu(game.menuSkin);
        loseMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_lose"));

        shopMenu = new ShopMenu(game.menuSkin);
        shopMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_shop"));

        shopPrimeMenu = new ShopPrimeMenu(game.menuSkin);
        shopPrimeMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_shop_prime"));

        settingsMenu = new SettingsMenu(game.menuSkin);
        settingsMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_settings"));

        projectsPopup = new ProjectsPopupMenu(game.menuSkin);
        projectsPopup.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_projects_popup"));

        objective = game.assetMngr.createSprite("plant_your_palm");
        objective.setPosition(500, 400);

        gameOver = false;
    }

    float time = 0;

    Sound s;

    int c = -1;

    @Override
    public void preAct(float delta) {
        if (first)
            game.sfx.play("sfx_dev_qst_plant_a_palm_tree", false);
        //            game.assetMngr.getSound("sfx_dev_qst_plant_a_palm_tree").play();
        time += delta;

        if (time > 1 && c == 0) {
            game.sfx.play("sfx_gl_stg_win_0_poa", false);
            //            game.assetMngr.getSound("sfx_gl_stg_win_0_poa").play();
            c++;
        }

        if (time > 2 && c == 1) {
            game.sfx.play("sfx_gl_stg_win_1_freshi", false);
            //            game.assetMngr.getSound("sfx_gl_stg_win_1_freshi").play();
            c++;
        }

        if (time > 3 && c == 2) {
            game.sfx.play("sfx_gl_stg_win_2_bomba_sana", false);
            //            game.assetMngr.getSound("sfx_gl_stg_win_2_bomba_sana").play();
            c++;
        }

        if (!ingameControl || !pAvatar.isAlive()) return;

        if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.D)) {
            if (pAvatar.getStatus() == Status.HIT
                            || pAvatar.getStatus() == Status.DIE)
                return;
            if (Gdx.input.isKeyPressed(Keys.A)) {
                pAvatar.setDirection(VJXDirection.Left);
            }
            if (Gdx.input.isKeyPressed(Keys.D)) {
                pAvatar.setDirection(VJXDirection.Right);
            }
            if (!jumpPressed)
                pAvatar.run();
        }

        game.getPlayer().getBanana().act(delta);
        game.getPlayer().getTotem().act(delta);
        game.getPlayer().getSun().act(delta);
    }

    @Override
    public void postAct(float delta) {
        //        if (!gameOver && !pAvatar.isAlive()) {
        //            lose();
        //        }

        if (jumpPressed) {
            chargeTime += delta;

            Vector2 bodyVel = pAvatar.getBody().getLinearVelocity();
            bodyVel.y = pAvatar.getJumpHeight();
            pAvatar.getBody().setLinearVelocity(bodyVel);

            if (chargeTime >= .25f) {
                jumpPressed = false;
                chargeTime = 0;
            }
        }

        Kawaida pA = pAvatar;

        //        if (pA.getCenter().x > game.getScreen().getLevel().getLvlWidth() - 5400
        //                        && !gameOver) {
        //            showDialog(pA, game.getScreen().getLevel().saidi);
        //            game.sfxMngr.stopAllSounds();
        //            //            pAvatar.stopAllSounds();
        //        }

        if (gameOver) {
            pAvatar.setStatus(Status.WALK);
            if (pA.getCenter().x > game.getScreen().getLevel().getLvlWidth()
                            - 4300)
                win();
        }
        first = false;
    }

    private boolean first = true;

    @Override
    public void preDraw() {
    }

    @Override
    public void postDraw() {

        if (time < 2 && !gameOver) {
            getBatch().begin();
            objective.draw(getBatch());
            getBatch().end();
        } else {

            //            if (!game.assetMngr.getMusic(
            //                            game.getScreen().getLevel().getBgMusicName())
            //                            .isPlaying() && last != loseMenu && last != winMenu)
            //                game.assetMngr.getMusic(
            //                                game.getScreen().getLevel().getBgMusicName())
            //                                .play();
        }
    }

    private boolean cancel() {
        if (!pAvatar.isSpawned() || pAvatar.getStatus().blocking)
            return true;

        return false;
    }

    @Override
    public boolean handle(Event event) {
        Actor actor = event.getTarget();
        //                    System.out.println("------------Handle change event----------");
        //                    System.out.println("Event      : " + event);
        //                    System.out.println("Actor      : " + actor.getName());
        //                    System.out.println("EventActor : " + eActor.getName());
        //                    System.out.println("EventTarget: " + target.getName());
        //                    System.out.println("-----------------------------------------");
        //                    System.out.println();

        if (cancel() || actor == null) return true;

        if (actor.getName().contains("joystick")) {
            Touchpad tp = (Touchpad) actor;

            float knobX = tp.getKnobPercentX();

            if (knobX < 0)
                pAvatar.setDirection(VJXDirection.Left);
            if (knobX > 0)
                pAvatar.setDirection(VJXDirection.Right);

            if (pAvatar.isOnGround()) {
                if (knobX == 0) {
                    pAvatar.stop();
                } else {
                    pAvatar.run();
                }
            }

            Body body = pAvatar.getBody();
            Vector2 bodyVel = body.getLinearVelocity();

            bodyVel.x = Math.abs(knobX) * pAvatar.getSpeed();
            body.setLinearVelocity(bodyVel);
        }
        return true;
    }

    private boolean ingameControl = true;

    @Override
    public boolean keyDown(int keycode) {

        if (ingameControl) {

            if (keycode == Keys.SPACE) {
                if (pAvatar.isOnGround()) {
                    jumpPressed = true;
                    pAvatar.jump();
                    return true;
                }
            }

            if (keycode == Keys.M) {
                pAvatar.throwCocos();
                uHUD.update();
                return true;
            }

            if (keycode == Keys.NUM_1) {
                pAvatar.setStatus(Status.IDLE);
                return true;
            }

            if (keycode == Keys.NUM_2) {
                pAvatar.setStatus(Status.HIT);
                return true;
            }

            if (keycode == Keys.NUM_3) {
                pAvatar.setStatus(Status.RUN);
                return true;
            }

            if (keycode == Keys.NUM_4) {
                pAvatar.setStatus(Status.DIE);
                return true;
            }

            if (keycode == Keys.NUM_5) {
                pAvatar.setStatus(Status.JUMP);
                return true;
            }

            if (keycode == Keys.NUM_6) {
                pAvatar.setStatus(Status.THROW);
                return true;
            }

            if (keycode == Keys.NUM_7) {
                pAvatar.setStatus(Status.WALK);
                return true;
            }
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (ingameControl) {
            if (keycode == Keys.A) {
                pAvatar.stop();
                return true;
            }

            if (keycode == Keys.D) {
                pAvatar.stop();
                return true;
            }
            if (keycode == Keys.SPACE) {
                if (jumpPressed) jumpPressed = false;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (ingameControl) {
            if (gameOver) return true;

            Vector2 v;
            screenToStageCoordinates(v = new Vector2(screenX, screenY));
            Actor a = hit(v.x, v.y, true);

            if (a == null) {

                if (v.x < getWidth() / 2) {
                    uHUD.setJoysticPos(v.x, v.y);
                    return super.touchDown(screenX, screenY, pointer, button);
                } else if (pAvatar.isOnGround()) {
                    jumpPressed = true;
                    pAvatar.jump();
                    return true;
                }
            }

            if (a == null) return true;

            if (a.getName().contains("joystick")) {
                return super.touchDown(screenX, screenY, pointer, button);
            }

            if (a.getName().equals("ui_btn_pause")) { return true; }

            if (button == 1) {
                pAvatar.throwCocos();
                uHUD.update();
                return true;
            }
        }

        return false;
    }

    private UiTable last;

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector2 v;
        screenToStageCoordinates(v = new Vector2(screenX, screenY));

        Actor a = hit(v.x, v.y, true);

        if (a == null) return super.touchUp(screenX, screenY, pointer, button);

        if (ingameControl) {

            if (a.getName().equals("ui_btn_pause")) {
                showPause();
                return true;
            }

            if (a.getName().equals("ui_btn_cocos")) {
                pAvatar.throwCocos();
                uHUD.update();
                return true;
            }

            super.touchUp((int) uHUD.getJoystick().getX(),
                            (int) uHUD.getJoystick().getY(),
                            pointer, button);

            if (v.x < getWidth() / 2) {
                super.touchUp(screenX, screenY, pointer, button);
            } else if (jumpPressed && !gameOver) {
                jumpPressed = false;
                return true;
            }
        } else {
            if (a.getName().equals("ui_btn_restart")) {
                //                if (pAvatar.isDead() && pAvatar.getTotem()
                //                                .getAmount() < 1) { return true; }
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());
                uHUD.getJoystick().addListener(this);

                game.getScreen().restart();
                pAvatar = game.getPlayer().getAvatar();

                ingameControl = true;
                gameOver = false;
                return true;
            }

            if (a.getName().equals("ui_btn_map_znz")
                            || a.getName().equals("ui_btn_map")) {
                clear();
                game.getScreen().loadMenu(a.getName());
                return true;
            }

            if (a.getName().equals("ui_btn_resume")) {
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());

                uHUD.getJoystick().addListener(this);

                pause(false);
                return true;
            }

            if (a.getName().equals("ui_btn_close")) {
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());

                uHUD.getJoystick().addListener(this);

                pause(false);
                return true;
            }

            if (a.getName().equals("ui_btn_settings")) {
                clear();

                settingsMenu.prepare();
                addActor(settingsMenu);

                last = pauseMenu;
                return true;
            }

            if (a.getName().equals("ui_btn_shop")) {
                clear();

                shopMenu.prepare();
                addActor(shopMenu);

                game.getScreen().pause();
                return true;
            }

            if (a.getName().equals("ui_btn_shop_prime")) {
                clear();

                shopPrimeMenu.prepare();
                addActor(shopPrimeMenu);

                return true;
            }

            if (a.getName().equals("ui_btn_back")) {
                shopMenu.remove();
                shopPrimeMenu.remove();
                settingsMenu.remove();
                projectsPopup.remove();

                if (last != null) {
                    last.prepare();
                    addActor(last);
                } else {
                    addActor(uHUD);
                    addActor(uHUD.getJoystick());

                    uHUD.getJoystick().addListener(this);
                    game.getScreen().resume();
                    ingameControl = true;
                }
                return true;
            }

            if (a.getName().equals("ui_btn_next")) {
                SpriteDrawable s = ingameMenu.getStoryDialog().nextSprite();
                ingameMenu.setBackground(s);

                if (s == null)
                    outro();
                return true;
            }

            if (a.getName().equals("ui_btn_exit")) {
                Gdx.app.exit();
            }
        }

        return false;
    }

    public void updateHUD() {
        uHUD.update();
    }
}