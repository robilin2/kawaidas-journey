package de.kawaida.kjgame.entities.items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.venjinx.core.libgdx.VJXUtility;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.entity.AnimatedEntity;
import de.venjinx.core.libgdx.entity.EntityDef;
import de.venjinx.core.libgdx.entity.NewEntity;


public class Item extends AnimatedEntity {

    private int amount = 0;
    private int maxAmount = 1;

    public Item(EntityDef def) {
        super(def, BodyType.StaticBody);
        b2dCategory = PhysCat.ITEM.id;
        b2dMask = PhysCat.PLAYER.id;

        if (def.getUniqueName().contains("wpn")) {
            b2dCategory |= PhysCat.WEAPON.id;
            b2dMask = VJXUtility.PhysCat.ENEMY.id;
            type = BodyType.DynamicBody;
        }
    }

    public void drawInstances() {

    }

    @Override
    public void collideWith(NewEntity other) {
        other.collideWith(this);
    }

    @Override
    public void trigger(Fixture triggerFixture) {
    }

    @Override
    protected void createFixtures() {
        boundFixture.setSensor(true);
        //        resizeBound();
    }

    public void incr() {
        amount = amount + 1 <= maxAmount|| maxAmount == -1 ? amount + 1 : amount;
    }

    public void decr() {
        amount = amount <= 0 ? 0 : amount - 1;
    }

    public void set(int amount) {
        if (amount < 0) this.amount = 0;
        else if (maxAmount == -1 || amount <= maxAmount) this.amount = amount;
        else this.amount = maxAmount;
    }

    public int getAmount() {
        return amount;
    }

    public void setMax(int newMax) {
        maxAmount = newMax;

        if (newMax >= 0) amount = amount > maxAmount ? maxAmount : amount;
    }

    public int getMax() {
        return maxAmount;
    }

    public boolean isEmpty() {
        return amount == 0;
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}