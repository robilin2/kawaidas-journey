package de.kawaida.kjgame.entities.items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.items.interfaces.Valuable;
import de.venjinx.core.libgdx.entity.EntityDef;

public class Totem extends Item implements Valuable {

    //    public Totem(String name) {
    //        super(name);
    //    }

    public Totem(EntityDef def) {
        super(def);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}