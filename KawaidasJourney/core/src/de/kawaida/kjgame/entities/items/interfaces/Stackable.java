package de.kawaida.kjgame.entities.items.interfaces;

public interface Stackable {

    public void setMax(int newMax);

    public int getMax();
}
