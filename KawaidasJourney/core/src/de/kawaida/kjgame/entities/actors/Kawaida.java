package de.kawaida.kjgame.entities.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.enemies.Trap;
import de.kawaida.kjgame.entities.interactive.PalmPot;
import de.kawaida.kjgame.entities.items.Item;
import de.kawaida.kjgame.entities.items.Weapon;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.kawaida.kjgame.interfaces.Visitor;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;
import de.venjinx.core.libgdx.entity.Character;
import de.venjinx.core.libgdx.entity.EntityDef;
import de.venjinx.core.libgdx.entity.NewEntity;

public class Kawaida extends Character implements Visitor {

    public Kawaida(VJXGame game, EntityDef def) {
        super(def, BodyType.DynamicBody);
        b2dCategory |= PhysCat.PLAYER.id;
        b2dMask |= (short) (PhysCat.ITEM.id | PhysCat.ENEMY.id);

        this.game = game;

        moveSpeed = 5f;
    }

    private VJXGame game;

    private float jumpHeight = 12f;

    private Weapon weapon;

    //    public Kawaida(VJXGame game, String who) {
    //        super(who, BodyType.DynamicBody);
    //        b2dCategory |= PhysCat.PLAYER.id;
    //        b2dMask |= (short) (PhysCat.ITEM.id | PhysCat.ENEMY.id);
    //
    //        setName("kawaida");
    //
    //        this.game = game;
    //
    //        moveSpeed = 5f;
    //    }

    public void jump() {
        setStatus(Status.JUMP);
    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
    }

    @Override
    public void collideWith(NewEntity other) {

        // player hits collectable
        if (other instanceof Item) {
            if (!other.getName().contains("palmPot")) {
                game.sfx.play("sfx_" + other.getName()
                                + "_collected", false);
                collect((Item) other);

                game.getScreen().getHUD().updateHUD();
            } else other.setStatus(Status.TRIGGERED);
            return;
        }

        // player hits enemy
        if (other instanceof Enemy) {
            if (!status.blocking)
                if (!attacking) {
                    setStatus(Status.HIT);
                    getBody().setLinearVelocity(0, 0);
                } else {
                    other.setStatus(Status.DIE);

                    Vector2 v = body.getLinearVelocity();
                    body.setLinearVelocity(v.x, 6f);
                    setStatus(Status.JUMP, true);
                    attacking = false;
                }
            return;
        }
        if (other instanceof PalmPot) {
            other.setStatus(Status.TRIGGERED);
            //            ((PalmTree) other).getAnimations().setNextAnimation("palm_grown");
            return;
        }
    }

    @Override
    public void trigger(Fixture triggerFixture) {
        VJXData fData = (VJXData) triggerFixture.getUserData();
        String name = fData.getName();
        if (name.contains("head")) {
            if (name.contains("trap")) {
                fData.getEntity().setStatus(Status.TRIGGERED);
                collideWith(fData.getEntity());
            } else attacking = true;
        }
    }

    public void collect(Item c) {
        c.despawn();

        if (c.getName().contains("banana")) {
            game.getPlayer().getBanana().incr();
            return;
        }
        if (c.getName().contains("sun")) {
            game.getPlayer().getSun().incr();
            return;
        }
        if (c.getName().contains("totem")) {
            game.getPlayer().getTotem().incr();
            return;
        }
        if (c.getName().contains("cocos")) {
            weapon.incr();
            return;
        }
    }

    public void throwCocos() {
        if (weapon.isEmpty()) return;
        setStatus(Status.THROW);

        if (direction == VJXDirection.Right) {
            weapon.setPosition(actor.getX() + actor.getWidth(),
                            actor.getY() + actor.getHeight());
            weapon.getBody().setLinearVelocity(10, 1);
        } else {
            weapon.setPosition(actor.getX(), actor.getY() + actor.getHeight());
            weapon.getBody().setLinearVelocity(-10, 1);
        }

        weapon.decr();
    }

    public void setWeapon(Weapon w) {
        weapon = w;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public float getJumpHeight() {
        return jumpHeight;
    }

    @Override
    public void visit(Acceptor acceptor) {
        // player visit collectable
        if (acceptor instanceof Item) {
            collect((Item) acceptor);
            game.getScreen().getLevel().removeEntity((Item) acceptor);
            game.getScreen().getHUD().updateHUD();
            return;
        }
    }

    @Override
    public void hit(Acceptor acceptor) {
        // player hits enemy
        if (acceptor instanceof Enemy) {
            ((Enemy) acceptor).setStatus(Status.DIE);

            Vector2 v = body.getLinearVelocity();
            body.setLinearVelocity(v.x, 6f);
            setStatus(Status.JUMP, true);
            attacking = false;

            if (!status.blocking) if (!attacking) {
            } else {
            }
            return;
        }

    }

    @Override
    public void trigger(Acceptor acceptor) {
        if (acceptor instanceof Trap) {
            ((Trap) acceptor).setStatus(Status.TRIGGERED);
            setStatus(Status.HIT);
            getBody().setLinearVelocity(0, 0);
        }
    }

    @Override
    public void spawnActor(World world) {

    }
}