package de.kawaida.kjgame.entities.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.venjinx.core.libgdx.entity.Character;
import de.venjinx.core.libgdx.entity.EntityDef;
import de.venjinx.core.libgdx.entity.NewEntity;

public class Neutral extends Character {

    //    public Neutral(String who) {
    //        super(who, BodyType.KinematicBody);
    //        // TODO Auto-generated constructor stub
    //    }

    public Neutral(EntityDef def) {
        super(def, BodyType.KinematicBody);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void collideWith(NewEntity other) {
        // TODO Auto-generated method stub

    }

    @Override
    public void trigger(Fixture triggerFixture) {
        // TODO Auto-generated method stub

    }

    @Override
    public void userAct(float deltaT) {
        // TODO Auto-generated method stub

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        // TODO Auto-generated method stub

    }

    @Override
    public void spawnActor(World world) {
        // TODO Auto-generated method stub

    }
}
