package de.kawaida.kjgame.entities.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.interfaces.Acceptor;
import de.kawaida.kjgame.interfaces.Visitor;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.entity.Character;
import de.venjinx.core.libgdx.entity.EntityDef;
import de.venjinx.core.libgdx.entity.NewEntity;

public class Enemy extends Character implements Visitor {

    //    public Enemy(String who) {
    //        super(who, BodyType.DynamicBody);
    //        b2dCategory |= PhysCat.ENEMY.id;
    //        b2dMask |= PhysCat.PLAYER.id | PhysCat.WEAPON.id;
    //    }

    public Enemy(EntityDef def) {
        super(def, BodyType.DynamicBody);
        b2dCategory |= PhysCat.ENEMY.id;
        b2dMask |= PhysCat.PLAYER.id | PhysCat.WEAPON.id;
    }

    @Override
    public void visit(Acceptor vjxE) {

    }

    @Override
    public void hit(Acceptor acceptor) {
        if (acceptor instanceof Kawaida) {
            Kawaida p = (Kawaida) acceptor;
            p.setStatus(Status.HIT);
            p.getBody().setLinearVelocity(0, 0);
        }
    }

    @Override
    public void trigger(Acceptor vjxE) {

    }

    @Override
    public void collideWith(NewEntity other) {
        if (other instanceof Kawaida)
            other.collideWith(this);
    }

    @Override
    public void trigger(Fixture triggerFixture) {
    }

    @Override
    public void userAct(float deltaT) {
        // TODO Auto-generated method stub

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        // TODO Auto-generated method stub

    }

    @Override
    public void spawnActor(World world) {
        // TODO Auto-generated method stub

    }
}