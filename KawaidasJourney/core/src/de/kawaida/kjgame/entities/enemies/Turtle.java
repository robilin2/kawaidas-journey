package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.actors.Kawaida;
import de.venjinx.core.libgdx.entity.EntityDef;

public class Turtle extends Enemy {

    //    public Turtle(String who) {
    //        super(who);
    //        setName("turtle");
    //    }

    public Turtle(EntityDef def) {
        super(def);
        actor.setName("turtle");
    }

    @Override
    public void userAct(float deltaT) {
        if (!isAlive()) return;

        Kawaida p = getWorld().getPlayerAvatar();
        //        setDirection(p.getX() < actor.getX() ? VJXDirection.Left
        //                        : VJXDirection.Right);

        body.setLinearVelocity(moveSpeed, body.getLinearVelocity().y + .12f);
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}