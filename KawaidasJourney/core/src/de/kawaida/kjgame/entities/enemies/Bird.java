package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.actors.Kawaida;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;
import de.venjinx.core.libgdx.entity.EntityDef;

public class Bird extends Enemy {

    public Bird(EntityDef def) {
        super(def);
        actor.setName("jz_bird1");

        moveSpeed = 1.5f;
    }

    private float posT = 0;
    private float waveLength = 50;
    private float amplitude = 1.5f;

    public long soundID;

    //    public Bird(String who) {
    //        super(who);
    //        setName("jz_bird1");
    //
    //        moveSpeed = 1.5f;
    //    }

    public void flap() {
        getBody().setLinearVelocity(moveSpeed, 1.8f);

        //        playSound("fx_jz_bird1_fly", false);
    }

    @Override
    public void userAct(float deltaT) {
        Kawaida p = getWorld().getPlayerAvatar();

        setDirection(p.getCenter().x < getCenter().x ? VJXDirection.Left
                        : VJXDirection.Right);

        float dstToPlayer = p.getCenter().dst(getCenter());

        if (dstToPlayer > 500) {
            setStatus(Status.IDLE, true);

            getBody().setLinearVelocity(0f, 0f);
//            stopSound("fx_jz_bird1_fly");
            return;
        } else {
            if (status != Status.FLY && status != Status.DIE) {
                setDirection(p.getCenter().x < getCenter().x
                                ? VJXDirection.Left : VJXDirection.Right);
                setStatus(Status.FLY);
                getBody().setGravityScale(.2f);
                flap();

                for (Fixture f : body.getFixtureList()) {
                    b2dMask &= ~PhysCat.NO_PASS.id;

                    Filter filter = new Filter();
                    filter.categoryBits = b2dCategory;
                    filter.maskBits = b2dMask;
                    f.setFilterData(filter);
                }
            }
        }

        if (status == Status.FLY) {
            //            if (p.getCenter().dst(getCenter()) > 700 || status == VJXStatus.DIE)
//                stopSound("fx_jz_bird1_fly");
        }
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}