package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.entity.EntityDef;

public class Trap extends Enemy {

    //    public Trap(String who) {
    //        super(who);
    //        type = BodyType.KinematicBody;
    //
    //        b2dCategory |= PhysCat.TRIGGER.id;
    //
    //        boundDef.isSensor = true;
    //    }

    public Trap(EntityDef def) {
        super(def);
        type = BodyType.KinematicBody;

        b2dCategory |= PhysCat.TRIGGER.id;

        boundDef.isSensor = true;
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}