package de.kawaida.kjgame.entities.level;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.actors.Kawaida;
import de.kawaida.kjgame.entities.interactive.PalmPot;
import de.kawaida.kjgame.entities.items.Weapon;
import de.kawaida.kjgame.entities.items.interfaces.Consumable;
import de.kawaida.kjgame.entities.items.interfaces.Valuable;
import de.kawaida.kjgame.entities.neutrals.Saidi;
import de.venjinx.core.libgdx.BodyEditorLoader;
import de.venjinx.core.libgdx.BodyEditorLoader.Model;
import de.venjinx.core.libgdx.BodyEditorLoader.RigidBodyModel;
import de.venjinx.core.libgdx.ParallaxCamera;
import de.venjinx.core.libgdx.Player;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.VJXUtility.Category;
import de.venjinx.core.libgdx.VJXUtility.Environment;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.entity.EntityDef;
import de.venjinx.core.libgdx.entity.EntityGroup;
import de.venjinx.core.libgdx.entity.NewEntity;
import de.venjinx.core.libgdx.entity.Obstacle;
import de.venjinx.core.libgdx.entity.Terrain;
import de.venjinx.core.libgdx.entity.TiledEntity;

public class GameWorld extends VJXStage {
    private OrthographicCamera cam;

    // parallax camera and calculation utility vectors
    private ParallaxCamera paraCam;
    private Matrix4 normalProjectionMatrix;
    private Matrix4 parallaxTransformatrix;

    private Vector3 curr = new Vector3();
    private Vector3 last = new Vector3(-1, -1, -1);
    private Vector3 delta = new Vector3();
    private float paraX = 0;

    private World b2dWorld;
    private OrthographicCamera b2dCam;
    private HashMap<String, BodyEditorLoader> bodyLoaders;

    private String environment;
    private String bgMusic;

    private TiledMap map;

    // map settings
    private int lvlWidth;
    private int lvlHeight;
    private int sunCount = 0;
    private int potCount = 0;
    private int evoLevel = 0;

    // parallax background layers
    private TiledMapTileLayer sky;
    private TiledMapTileLayer horizonFar;
    private TiledMapTileLayer horizonNear;
    private TiledMapTileLayer worldPath;

    // tiled map layers
    private MapLayer worldEntities;
    private TiledMapTileLayer worldTerrain;
    private ArrayList<TiledMapTileLayer> bgLayers;
    private ArrayList<TiledMapTileLayer> fgLayers;

    // parallax foreground layers
    private TiledMapTileLayer foreground;
    private TiledMapTileLayer foregroundDec;

    // evolution tile set
    private TiledMapTileSet evoTilesBase;

    private OrthogonalTiledMapRenderer tmr;

    private HashMap<Long, NewEntity> entities;
    private ArrayDeque<Long> removeIPs;

    // in-game scene graph nodes
    private EntityGroup gameWorldNode;

    private EntityGroup groundNode;

    private EntityGroup obstaclesNode;

    private EntityGroup charactersNode;
    private EntityGroup enemiesNode;

    private EntityGroup itemsNode;
    private EntityGroup valuablesNode;
    private EntityGroup consumableNode;

    private EntityGroup usablesNode;
    private EntityGroup weaponsNode;
    private EntityGroup toolsNode;

    private Terrain collision;

    public Saidi saidi;
    public PalmPot palm;

    private Kawaida pAvatar;

    public GameWorld(VJXGame game) {
        super(game);
        setName("default-empty-map");
        getRoot().setName("node_scene_root");

        normalProjectionMatrix = new Matrix4();
        parallaxTransformatrix = new Matrix4();

        bgMusic = "none";

        bgLayers = new ArrayList<>();
        fgLayers = new ArrayList<>();

        evoTilesBase = new TiledMapTileSet();

        entities = new HashMap<>();
        removeIPs = new ArrayDeque<Long>();

        cam = game.getCam();

        paraCam = new ParallaxCamera(game.targetRes.x, game.targetRes.y);
        b2dCam = new OrthographicCamera();
        b2dCam.setToOrtho(false, game.getView().getWorldWidth() / 100f,
                        game.getView().getWorldHeight() / 100f);

        bodyLoaders = new HashMap<>();

        gameWorldNode = new EntityGroup("node_gameWorld");

        // scene graph nodes for level ground and obstacles
        groundNode = new EntityGroup("node_ground");
        obstaclesNode = new EntityGroup("node_obstacles");

        // scene graph node for characters
        charactersNode = new EntityGroup("node_characters");

        enemiesNode = new EntityGroup("node_enemies");
        charactersNode.addActor(enemiesNode);

        // scene graph node for items
        itemsNode = new EntityGroup("node_items");

        valuablesNode = new EntityGroup("node_valuables");
        consumableNode = new EntityGroup("node_consumables");

        itemsNode.addActor(valuablesNode);
        itemsNode.addActor(consumableNode);

        // scene graph node for usable items
        usablesNode = new EntityGroup("node_usables");

        toolsNode = new EntityGroup("node_tools");
        weaponsNode = new EntityGroup("node_weapons");

        usablesNode.addActor(weaponsNode);
        usablesNode.addActor(toolsNode);
        itemsNode.addActor(usablesNode);

        // add level root nodes to gameWorld node
        gameWorldNode.addActor(obstaclesNode);
        gameWorldNode.addActor(groundNode);
        gameWorldNode.addActor(charactersNode);
        gameWorldNode.addActor(itemsNode);

        addActor(gameWorldNode);

        tmr = new OrthogonalTiledMapRenderer(new TiledMap(), getBatch());
    }

    @Override
    protected void init() {
        b2dWorld = new World(new Vector2(0f, -9.81f), true);
        b2dWorld.setContactListener(game.cCtrl);

        game.getScreen().getDebug().setB2DWorld(b2dWorld);

        // load definitions for entities
        for (EntityDef eDef : entityDefs.values()) {
            eDef.loadBodyDef();
        }

        // load bodies for global items
        BodyEditorLoader loader = new BodyEditorLoader(
                        Gdx.files.internal("global/item/bodies_gl_items.def"));
        bodyLoaders.put("bodies_gl_items", loader);

        loader = new BodyEditorLoader(Gdx.files
                        .internal("development/terrain/dev_trn_colision.bodies"));
        bodyLoaders.put("bodies_dev_terrain", loader);

        loader = new BodyEditorLoader(Gdx.files
                        .internal("kizim_kazi/terrain/kzk_trn_world.bodies"));
        bodyLoaders.put("bodies_dev_terrain", loader);

        restart();
    }

    @Override
    public void preAct(float delta) {
        //        timePassed += delta;

        //        if (timePassed >= step) {
        //        b2dWorld.step(delta, 6, 2);

        //        for (NewEntity e : entities.values()) {
        //            e.act(delta);
        //        }
        //            timePassed -= step;
        //        }
    }

    @Override
    public void postAct(float delta) {
        b2dWorld.step(delta, 6, 2);

        for (NewEntity e : entities.values()) {
            e.act(delta);

            if (e.getB2DCategory() == PhysCat.REMOVE.id) {
                e.getBody().setType(BodyType.DynamicBody);
                if (e.getPosition().y < -200) removeEntity(e);
            }
        }
    }

    @Override
    public void preDraw() {
        getBatch().begin();

        normalProjectionMatrix.set(getBatch().getProjectionMatrix());

        Vector2 v = new Vector2(game.getPlayer().getAvatar().getCenter());
        v.x = v.x > game.targetRes.x / 2 ? v.x : game.targetRes.x / 2;
        v.y = v.y > game.targetRes.y / 2 ? v.y : game.targetRes.y / 2;
        v.scl(.01f);

        b2dCam.position.set(v, b2dCam.position.z);
        b2dCam.update();

        cam.position.set(v.scl(100), cam.position.z);
        cam.update();

        drawBackground();

        for (TiledMapTileLayer t : bgLayers)
            tmr.renderTileLayer(t);

        tmr.renderTileLayer(worldTerrain);

        getBatch().setProjectionMatrix(normalProjectionMatrix);

        getBatch().end();
    }

    @Override
    public void postDraw() {
        getBatch().begin();

        normalProjectionMatrix.set(getBatch().getProjectionMatrix());

        parallaxTransformatrix.idt();

        Matrix4 trans = new Matrix4();
        Vector3 viewSize = new Vector3(cam.viewportWidth * cam.zoom,
                        cam.viewportWidth * cam.zoom, 0);

        Vector3 pos = new Vector3(cam.position.x - viewSize.x / 2f,
                        cam.position.y - viewSize.y / 2f, 0);

        trans.idt();
        paraX = -(getWidth() - getWidth() * 1.1f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * 1.1f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(
                        paraCam.calculateParallaxMatrix(1.1f, 1));
        //        tmr.renderTileLayer(pathBG);

        //        paraX = -(getWidth() - getWidth() * 1.17f) / 2;
        //        parallaxTransformatrix.translate(paraX, 0, 0);
        //        getBatch().setTransformMatrix(parallaxTransformatrix);
        //        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(1.17f, 1));

        //        tmr.setView(cam.combined, pos.x * 1.17f, pos.y, viewSize.x, viewSize.y);

        tmr.renderTileLayer(foreground);
        tmr.renderTileLayer(foregroundDec);

        for (TiledMapTileLayer t : fgLayers)
            tmr.renderTileLayer(t);

        getBatch().setProjectionMatrix(normalProjectionMatrix);

        getBatch().end();
        clearWorld(false);

        //        game.pause();
    }

    public void restart() {
        clearWorld(true);

        loadObjects();
        loadTerrain();

        game.assetMngr.getMusic(bgMusic).setVolume(1f);
    }

    public void load(String path, boolean internal) {

        path.replace("\\", "/");

        String[] sa = path.split("/");
        String lvlName = sa[sa.length - 1].split("\\.")[0];
        setName(lvlName);

        map = game.assetMngr.loadMap(path, lvlName, internal);
        bgMusic = map.getProperties().get("music", String.class);
        environment = map.getProperties().get("environment", String.class);

        tmr.setMap(map);

        Array<TiledMapTileLayer> layers = map.getLayers()
                        .getByType(TiledMapTileLayer.class);

        for (TiledMapTileLayer t : layers) {
            if (t.getName().contains("deco")) bgLayers.add(t);
            else if (t.getName().contains("deco")) fgLayers.add(t);
        }

        sky = (TiledMapTileLayer) map.getLayers().get("sky");
        foreground = (TiledMapTileLayer) map.getLayers().get("foreground");
        foregroundDec = (TiledMapTileLayer) map.getLayers()
                        .get("foreground_deco#0");
        horizonFar = (TiledMapTileLayer) map.getLayers().get("horizon_far");
        horizonNear = (TiledMapTileLayer) map.getLayers().get("horizon_near");
        worldPath = (TiledMapTileLayer) map.getLayers().get("world_path");

        worldTerrain = (TiledMapTileLayer) map.getLayers().get("world_terrain");
        worldEntities = map.getLayers().get("world_entities");


        evoTilesBase = map.getTileSets().getTileSet("tiles_"
                                        + Environment.get(environment).envShort
                                        + "_evolution_0");

        lvlWidth = getMapWidth() * getTileWidth();
        lvlHeight = getMapHeight() * getTileHeight();

        setupAssetDefs();
    }

    public void addEntity(NewEntity entity, boolean addRoot) {
        registerEntity(entity);

        entity.spawn(b2dWorld);

        if (entity instanceof Enemy) {
            enemiesNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Kawaida) {
            charactersNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Valuable) {
            valuablesNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Consumable) {
            consumableNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Weapon) {
            weaponsNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Terrain) {
            groundNode.addActor(entity.getActor());
            return;
        }
        if (entity instanceof Obstacle) {
            obstaclesNode.addActor(entity.getActor());
            return;
        }

        addActor(entity.getActor());
    }

    public void removeEntity(NewEntity entity) {
        if (entity == null) return;

        entity.despawn();

        if (entity.getName().contains("sun")) sunCount--;
    }

    public NewEntity getEntity(long id) {
        return entities.get(id);
    }

    public Kawaida getPlayerAvatar() {
        return pAvatar;
    }

    protected void registerEntity(NewEntity e) {
        entities.put(e.getID(), e);
        e.getActor().addListener(game.entityListener);
    }

    protected void unregisterEntity(NewEntity e) {
        unregisterEntity(e.getID());
    }

    protected void unregisterEntity(long id) {
        if (!entities.containsKey(id)) return;
        entities.remove(id).getActor().removeListener(game.entityListener);
    }

    private void loadObjects() {
        Vector2 pos = new Vector2();
        TiledMapTile tile;
        NewEntity object;
        MapProperties mp;
        TiledMapTileSets tileSets = map.getTileSets();

        MapObjects objects = worldEntities.getObjects();
        for (MapObject mo : objects) {
            if (mo instanceof TextureMapObject) {
                mp = mo.getProperties();
                tile = tileSets.getTile(mp.get("gid", Integer.class));

                EntityDef eDef = entityDefs.get(mp.get("gid", Integer.class));

                object = game.factory.createEntity(eDef);

                pos.set(mp.get("x", Float.class), mp.get("y", Float.class));
                //                System.out.println (object.getName());
                //                System.out.println(pos);
                //                pos.x += object.getWidth() / 2f;
                //                pos.y += getTileHeight();
                //                pos.y += object.getHeight();

                object.setPosition(pos.x, pos.y);

                if (object.getName().contains("palmPot")) potCount++;

                if (object.getName().contains("sun")) sunCount++;

                if (object.getName().contains("ground")) {
                    collision = (Terrain) object;
                    collision.setPosition(0, 0);
                    //                    System.out.println(collision.getPosition());
                    //                    System.out.println(collision.getBody().getPosition());
                }

                addEntity(object, false);

                if (object instanceof Kawaida) {
                    Player p = game.getPlayer();
                    pAvatar = (Kawaida) object;

                    pAvatar.setHealth(1);
                    pAvatar.getBody().setGravityScale(4);

                    p.setAvatar(pAvatar);
                    p.getBanana().set(1);
                    p.getBanana().setMax(4);
                    p.getTotem().set(0);

                    eDef = EntityDef.fromUniqueName("gl_wpn_cocos");
                    p.setWeapon((Weapon) game.factory.createEntity(eDef));
                    p.getWeapon().setMax(10);
                    p.getWeapon().set(0);

                    addEntity(p.getWeapon(), false);
                }
            }
        }
    }

    private void loadTerrain() {
        //        TiledMapTile t;

        MapProperties mp = map.getProperties();
        //        Environment env;
        Category cat;
        String name, attr, sCat;
        //        TiledMapTileSets tileSets = map.getTileSets();
        //        MapObjects objects = terrain.getObjects();
        BodyEditorLoader bLoader = bodyLoaders.get("bodies_dev_terrain");

        Cell c;
        boolean[][] checked = new boolean[worldTerrain.getWidth()][worldTerrain.getHeight()];
        for (int n = 0; n < worldTerrain.getWidth(); n++) {
            for (int m = 0; m < worldTerrain.getHeight(); m++) {
                if (checked[n][m]) continue;
                checked[n][m] = true;

                c = worldTerrain.getCell(n, m);

                if (c == null) continue;

                mp = c.getTile().getProperties();
                name = mp.get("name", String.class);
                //                env = Environment.get(mp.get("environment", String.class));
                cat = Category.get(mp.get("category", String.class));
                attr = mp.get("attributes", String.class);

                sCat = mp.get("subcategory", String.class);

                Array<Vector2> vertices = new Array<>();
                if (name == null) continue;

                if (sCat.contains("utility")) {
                    EntityDef def = new EntityDef(mp);

                    Model model = bLoader.getInternalModel();

                    HashMap<String, RigidBodyModel> map = (HashMap<String, RigidBodyModel>) model.rigidBodies;

                    name = "tile_" + def.getUniqueName() + "_"
                                    + def.getAttributes() + ".png";

                    RigidBodyModel rbModel = map.get(name);

                    //                    System.out.println();
                    //                    Vector2 offset = new Vector2();
                    //                    for (PolygonModel pm : rbModel.polygons) {
                    //                        for (Vector2 v : pm.vertices) {
                    //                            offset.set(n, m);
                    //                            System.out.println("off " + offset);
                    //                            System.out.println("v" + v);
                    //                            v.add(offset);
                    //                            System.out.println(v);
                    //                        }
                    //                    }

                    rbModel.origin.set(-n, -m);

                    FixtureDef fd = new FixtureDef();
                    fd.friction = 0;
                    fd.restitution = 0;
                    fd.filter.categoryBits = collision.getB2DCategory();
                    fd.filter.maskBits = collision.getCollisionMask();
                    //                    fd.filter.maskBits = VJXUtility.PhysCat.NONE.id;dddd

                    bLoader.attachFixture(collision.getBody(), name, fd, 1.28f);

                    Array<Fixture> fList = collision.getBody().getFixtureList();
                    Fixture f = fList.peek();
                    f.setUserData(new VJXData(def.getWho(), collision));
                }

                if (cat == Category.TERRAIN) {
                    //                    EntityDef def = new EntityDef(mp);
                    //
                    //                    Model model = bLoader.getInternalModel();
                    //
                    //                    HashMap<String, RigidBodyModel> map = (HashMap<String, RigidBodyModel>) model.rigidBodies;
                    //
                    //                    name = "tile_" + def.getUniqueName() + "_"
                    //                                    + def.getAttributes() + ".png";
                    //
                    //                    RigidBodyModel rbModel = map.get(name);
                    //
                    //                    //                    System.out.println();
                    //                    //                    Vector2 offset = new Vector2();
                    //                    //                    for (PolygonModel pm : rbModel.polygons) {
                    //                    //                        for (Vector2 v : pm.vertices) {
                    //                    //                            offset.set(n, m);
                    //                    //                            System.out.println("off " + offset);
                    //                    //                            System.out.println("v" + v);
                    //                    //                            v.add(offset);
                    //                    //                            System.out.println(v);
                    //                    //                        }
                    //                    //                    }
                    //
                    //                    rbModel.origin.set(-n, -m);
                    //
                    //                    FixtureDef fd = new FixtureDef();
                    //                    fd.friction = 0;
                    //                    fd.restitution = 0;
                    //                    fd.filter.categoryBits = collision.getB2DCategory();
                    //                    fd.filter.maskBits = collision.getCollisionMask();
                    //                    //                    fd.filter.maskBits = VJXUtility.PhysCat.NONE.id;dddd
                    //
                    //                    bLoader.attachFixture(collision.getBody(), name, fd, 1.28f);
                    //
                    //                    Array<Fixture> fList = collision.getBody().getFixtureList();
                    //                    Fixture f = fList.peek();
                    //                    f.setUserData(new VJXData(def.getWho(), collision));
                }

                if (sCat.contains("tiled")) {
                    createTiledObject(n, m, checked);
                    continue;
                }

                if (name.contains("platform")) {
                    createPlatform(n, m, mp, checked);
                    continue;
                }

                if (name.contains("tree")) {
                    if (attr.contains("crown_top")) {
                        vertices.add(new Vector2(n * getTileWidth() - 85,
                                        m * getTileHeight() + 32));
                        vertices.add(new Vector2((n + 1) * getTileWidth() + 85,
                                        m * getTileHeight() + 32));

                        if (n - 1 >= 0) checked[n - 1][m] = true;
                        if (n + 1 < getMapWidth()) checked[n + 1][m] = true;

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        collision.addVertices(v);
                        continue;
                    }
                    if (attr.contains("arm_left")) {
                        vertices.add(new Vector2(n * getTileWidth() + 10,
                                        m * getTileHeight() + 110));
                        vertices.add(new Vector2((n + 1) * getTileWidth() - 10,
                                        m * getTileHeight() + 110));

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        collision.addVertices(v);
                        continue;
                    }
                    if (attr.contains("arm_right")) {
                        vertices.add(new Vector2(n * getTileWidth() + 10,
                                        m * getTileHeight() + 95));
                        vertices.add(new Vector2((n + 1) * getTileWidth() - 10,
                                        m * getTileHeight() + 95));

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        collision.addVertices(v);
                        continue;
                    }
                }

                if (attr.contains("evo")) {
                    evolveTiles.add(c.getTile());
                }
            }
        }
    }

    private void createTiledObject(int n, int m, boolean[][] checked) {
        Cell c = worldTerrain.getCell(n, m);
        MapProperties props = c.getTile().getProperties();
        Environment env = Environment.get(props.get("environment", String.class));
        Category cat = Category.get(props.get("category", String.class));

        EntityDef def = EntityDef.fromUniqueName(
                        env.envShort + "_" + cat.catShort + "_" + props.get("name", String.class));

        TiledEntity te = (TiledEntity) game.factory.createEntity(def);

        Vector2 origin = new Vector2(n, m);
        Array<Cell> cells = new Array<>();
        findCells(n, m, checked, origin, cells);
        te.setCells(cells);
    }

    private void findCells(int n, int m, boolean[][] checked, Vector2 origin,
                    Array<Cell> cells) {
        if (checked[n][m]) return;

        checked[n][m] = true;

        Cell c = worldTerrain.getCell(n, m);
        c.getTile().setOffsetX((n - origin.x) * getTileWidth());
        c.getTile().setOffsetY((m - origin.y) * getTileHeight());
        cells.add(c);

        String neighbors;
        neighbors = c.getTile().getProperties().get("neighbors", String.class);

        if (neighbors.contains("left") && n - 1 >= 0)
            findCells(n - 1, m, checked, origin, cells);

        if (neighbors.contains("down") && m - 1 >= 0)
            findCells(n, m - 1, checked, origin, cells);

        if (neighbors.contains("right") && n + 1 < checked.length)
            findCells(n + 1, m, checked, origin, cells);

        if (neighbors.contains("up") && m + 1 < checked[0].length)
            findCells(n, m + 1, checked, origin, cells);
    }

    private void createPlatform(int n, int m, MapProperties mp,
                    boolean[][] checked) {
        String name;
        Environment env;
        Category cat;
        String attr;

        Cell c;
        name = mp.get("name", String.class);
        env = Environment.get(mp.get("environment", String.class));
        cat = Category.get(mp.get("category", String.class));
        attr = mp.get("attributes", String.class);

        Array<Vector2> vertices = new Array<Vector2>();

        EntityDef def = EntityDef.fromUniqueName(
                        env.envShort + "_" + cat.catShort + "_" + name);

        Obstacle platform = (Obstacle) game.factory.createEntity(def);

        addEntity(platform, false);

        //        Obstacle platform = new Obstacle(0, 0,
        //                        entityDefs.get(mp.get("gid", Integer.class)));

        if (attr.contains("left")) {
            vertices.add(new Vector2(n * getTileWidth() + 28,
                            m * getTileHeight() + 100));
            //                        vertices.add(new Vector2(n * getTileWidth() + 64,
            //                                        m * getTileHeight() + 100));
            //                        vertices.add(new Vector2(n * getTileWidth() + 128,
            //                                        m * getTileHeight() + 100));
            float x = n * getTileWidth() + 128;

            int tmpN = n + 1;
            c = worldTerrain.getCell(tmpN, m);
            while (c != null) {
                mp = c.getTile().getProperties();
                name = mp.get("name", String.class);
                //                            env = Environment.get(mp.get("environment", String.class));
                //                            cat = Category.get(mp.get("category", String.class));
                attr = mp.get("attributes", String.class);

                if (name != null) if (name.contains("platform")) {
                    checked[tmpN++][m] = true;
                    if (attr.contains("mid")) {
                        x += 128;
                        //                                    vertices.add(new Vector2(
                        //                                                    tmpN * getTileWidth() + 64,
                        //                                                    m * getTileHeight() + 100));
                        //                                    vertices.add(new Vector2(
                        //                                                    tmpN++ * getTileWidth() + 128,
                        //                                                    m * getTileHeight() + 100));
                        c = worldTerrain.getCell(tmpN, m);
                    }
                    if (attr.contains("right")) {
                        x += 90;
                        //                                    vertices.add(new Vector2(
                        //                                                    tmpN * getTileWidth() + 64,
                        //                                                    m * getTileHeight() + 100));
                        //                                    vertices.add(new Vector2(
                        //                                                    tmpN++ * getTileWidth() + 90,
                        //                                                    m * getTileHeight() + 100));
                        c = null;
                    }
                }
            }
            vertices.add(new Vector2(x, m * getTileHeight() + 100));

            Vector2[] v = new Vector2[vertices.size];
            for (int i = 0; i < vertices.size; i++) {
                v[i] = vertices.get(i).scl(1 / 100f);
            }
            platform.addVertices(v);
        }
    }

    private HashSet<TiledMapTile> evolveTiles = new HashSet<>();
    private HashMap<Integer, EntityDef> entityDefs = new HashMap<>();
    private HashSet<Integer> idsToLoad = new HashSet<>();

    private void setupAssetDefs() {
        String name, dir;
        resources.clear();

        // evolution tiles
        dir = environment + "/sheets/";
        name = "tiles_" + Environment.get(environment).envShort + "_evolution_0";
        resources.put(name, new AssetDescriptor<>(dir + name + ".png", Texture.class));

        dir = environment + "/sheets/";
        name = "tiles_" + Environment.get(environment).envShort + "_evolution_1";
        resources.put(name, new AssetDescriptor<>(dir + name + ".png", Texture.class));

        dir = environment + "/sheets/";
        name = "tiles_" + Environment.get(environment).envShort + "_evolution_2";
        resources.put(name, new AssetDescriptor<>(dir + name + ".png", Texture.class));

        TiledMapTile t;

        MapProperties mp;
        EntityDef eDef;
        TiledMapTileSets tileSets = map.getTileSets();

        MapObjects objects = worldEntities.getObjects();
        int tileID = -1;
        for (MapObject mo : objects) {
            mp = mo.getProperties();
            tileID = mp.get("gid", Integer.class);
            t = tileSets.getTile(tileID);

            mp.put("y", mp.get("y", Float.class) + getTileHeight());

            if (idsToLoad.add(tileID)) {
                eDef = new EntityDef(t.getProperties());
                entityDefs.put(tileID, eDef);
                resources.putAll(eDef.getAssetDefs());
            }
        }

        // controls
        dir = "user_interface/controls/";
        name = "controls";
        resources.put(name, new AssetDescriptor<>(dir + name + ".atlas",
                        TextureAtlas.class));

        // user interface icons
        dir = "global/sheets/";
        name = "gl_stage";
        resources.put(name, new AssetDescriptor<>(dir + "gl_stage.atlas",
                        TextureAtlas.class));

        // game sounds
        dir = "global/stage/sfx/";
        name = "sfx_gl_stg_win_jingle";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        name = "sfx_gl_stg_lose_jingle";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        name = "sfx_gl_stg_win_0_poa";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        name = "sfx_gl_stg_win_1_freshi";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        name = "sfx_gl_stg_win_2_bomba_sana";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        // quest sounds
        dir = environment + "/quest/sfx/";
        name = "sfx_dev_qst_plant_a_palm_tree";
        resources.put(name, new AssetDescriptor<>(dir + name + ".mp3",
                        Sound.class));

        // player resources
        game.getPlayer().fetchRes(resources);

        // music
        String music = map.getProperties().get("music", String.class);
        Environment env;
        //        Category cat;

        String[] str = music.split("_");
        env = Environment.get(str[1]);
        //        cat = Category.get(str[2]);

        if (music != null) {

            //            String mPath = "music/levels/" + music + ".mp3";
            String mPath = env.envName + "/stage/music/" + music + ".mp3";
            //            game.assetMngr.loadMusic(mPath, music);

            resources.put(music, new AssetDescriptor<>(mPath, Music.class));
        }
    }

    private void drawBackground() {
        paraCam.position.set(cam.position);
        //        paraCam.update();

        paraCam.unproject(curr.set(cam.position.x, cam.position.y, 0));
        if (!(last.x == -1 && last.y == -1 && last.z == -1)) {
            paraCam.unproject(delta.set(last.x, last.y, 0));
            delta.sub(curr);
            paraCam.position.add(delta.x, 0, 0);
        }
        last.set(cam.position.x, cam.position.y, 0);

        Matrix4 trans = new Matrix4();

        Vector3 viewSize = new Vector3(cam.viewportWidth * cam.zoom,
                        cam.viewportWidth * cam.zoom, 0);

        Vector3 pos = new Vector3(cam.position.x - viewSize.x / 2f,
                        cam.position.y - viewSize.y / 2f, 0);

        trans = new Matrix4();
        paraX = -(getWidth() - getWidth() * .8f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * .8f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(.8f, 1));
        tmr.renderTileLayer(sky);

        trans.idt();
        paraX = -(getWidth() - getWidth() * .9f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * .9f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(.9f, 1));
        tmr.renderTileLayer(horizonFar);
        tmr.renderTileLayer(horizonNear);

        trans.idt();
        paraX = -(getWidth() - getWidth() * 1f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(1f, 1));
        tmr.renderTileLayer(worldPath);

    }

    private void clearWorld(boolean all) {
        if (all) removeIPs.addAll(entities.keySet());
        else
            for (NewEntity e : entities.values())
                if (!e.isAlive()) removeIPs.add(e.getID());

        NewEntity e;
        while (!removeIPs.isEmpty()) {
            e = entities.get(removeIPs.removeLast());
            unregisterEntity(e);
            game.factory.destroyEntity(e);
        }
    }

    public int getTileWidth() {
        return map.getProperties().get("tilewidth", Integer.class);
    }

    public int getTileHeight() {
        return map.getProperties().get("tileheight", Integer.class);
    }

    public int getMapWidth() {
        return map.getProperties().get("width", Integer.class);
    }

    public int getMapHeight() {
        return map.getProperties().get("height", Integer.class);
    }

    public float getLvlWidth() {
        return lvlWidth;
    }

    public float getLvlHeight() {
        return lvlHeight;
    }

    public String getBgMusicName() {
        return bgMusic;
    }

    public World getB2DWorld() {
        return b2dWorld;
    }

    public OrthographicCamera getB2DCam() {
        return b2dCam;
    }

    public int getSunCount() {
        return sunCount;
    }

    public int getPotCount() {
        return potCount;
    }

    int activatedPots = 0;

    public void updateEvoLevel() {
        activatedPots++;
        float newLevel = (float) activatedPots / potCount;
        newLevel = 3 * newLevel - 1;

        for (TiledMapTile t : evoTilesBase) {
            t.getTextureRegion().setTexture(game.assetMngr.getTexture("tiles_"
                            + Environment.get(environment).envShort
                            + "_evolution_" + (int) newLevel));
        }

        evoLevel = (int) newLevel;
    }

    public void setEvoLevel(int level) {
        for (TiledMapTile t : evoTilesBase) {
            t.getTextureRegion().setTexture(game.assetMngr.getTexture("tiles_"
                            + Environment.get(environment).envShort
                                            + "_evolution_" + level));
        }

        evoLevel = level;
    }

    public int getEvoLevel() {
        return evoLevel;
    }

    @Override
    public void dispose() {
        clear();

        game.assetMngr.disposeMap(getName());

        bgLayers.clear();
        fgLayers.clear();

        //        enemyLayer = null;
        worldEntities = null;
        //        collectableLayer = null;
        worldTerrain = null;

        //        path.dispose();
        //        path = null;
        worldPath = null;

        //        sky.dispose();
        //        sky = null;
        sky = null;

        //        horizon.dispose();
        //        horizon = null;
        horizonFar = null;

        //        foreground.dispose();
        //        foreground = null;

        bgMusic = null;
        environment = null;

        clearWorld(true);

        map.dispose();
        map = null;

        entities = null;

        if (b2dWorld != null)
        b2dWorld.dispose();
        tmr.dispose();

        cam = null;
        paraCam = null;
        last = null;
        curr = null;
        delta = null;
        normalProjectionMatrix = null;

        super.dispose();
    }
}