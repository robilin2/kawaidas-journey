package de.kawaida.kjgame.interfaces;


public interface Collidable {

    public void collideWith(Collidable other);
}