package de.kawaida.kjgame.interfaces;

public interface Visitor {

    public void visit(Acceptor vjxE);

    public void hit(Acceptor vjxE);

    public void trigger(Acceptor vjxE);

}
