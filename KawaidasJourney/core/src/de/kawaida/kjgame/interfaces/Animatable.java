package de.kawaida.kjgame.interfaces;

import de.kawaida.kjgame.animation.AnimationSet;

public interface Animatable {

    public void setAnimations(AnimationSet ta);

    void stepAnim(float step);

}