package de.kawaida.kjgame.animation;

import com.badlogic.gdx.scenes.scene2d.Event;

import de.venjinx.core.libgdx.entity.AnimatedEntity;

public class Animation {

    public class AnimationEvent extends Event {

        public Animation animation;
        public String name = "";

        public AnimationEvent(Animation anim, String eventName) {
            animation = anim;
            name = anim.getName() + "_" + eventName;
        }

        public String getName() {
            return name;
        }

        public Animation getAnimation() {
            return animation;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private AnimatedEntity who;

    private String actorName;
    private String what;

    private float time;
    private float delay;
    private int frameCount = 0;
    private int timesPlayed = 0;
    private int repeat = 1;

    private int currentFrame = -1;
    private int nextFrame = 0;

    private int startFrame = 0, endFrame = 0;

    private boolean loop = false;
    private boolean loopBack = false;
    private boolean reverse = false;
    private boolean xFlipped = false;
    private boolean yFlipped = false;

    public Animation(AnimatedEntity who, String what) {
        this.who = who;
        actorName = who.getName();
        this.what = what;
        delay = 1 / 5f;
    }

    public void setDelay(float delay) {
        this.delay = delay;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean looping() {
        return loop;
    }

    public boolean loopingBack() {
        return loopBack;
    }

    public void setLoopBack(boolean loopBack) {
        this.loopBack = loopBack;
    }

    public void update(float deltaT) {
        if (delay <= 0 || frameCount < 2) return;
        time += deltaT;
        if (time >= delay) {
            update();
            time -= delay;
        }
    }

    public boolean isFinished() {
        return !loop && !(timesPlayed < repeat);
    }

    private void update() {
        if (isFinished()) return;

        // remove offset
        int normStart = currentFrame - startFrame;

        // get next frame based on running direction and total amount of frames
        nextFrame = (normStart + (reverse ? -1 : 1)) % frameCount;

        // reset offset
        nextFrame += startFrame;

        // Fix negative position when running backwards
        if (nextFrame < startFrame) nextFrame = endFrame - 1;

        AnimationEvent ae;
        if (!loopBack) {
            if (nextFrame == (reverse ? endFrame - 1 : startFrame)) {
                timesPlayed++;

                ae = new AnimationEvent(this, "restart");
                ae.setTarget(who.getActor());
                who.getActor().notify(ae, false);

                if (isFinished()) {
                    ae = new AnimationEvent(this, "finished");
                    ae.setTarget(who.getActor());
                    who.getActor().notify(ae, false);
                    return;
                }
            }
        } else {
            if (nextFrame == (reverse ? endFrame - 1 : startFrame)) {
                if (nextFrame == endFrame - 1 && reverse) {
                    timesPlayed++;

                    ae = new AnimationEvent(this, "restart");
                    ae.setTarget(who.getActor());
                    who.getActor().notify(ae, false);
                }

                reverse = !reverse;
                nextFrame = currentFrame + (reverse ? -1 : 1);
                return;
            }
        }
        currentFrame = nextFrame;
    }

    public void reset() {
        time = 0;
        currentFrame = startFrame;
        timesPlayed = 0;
        reverse = false;
        nextFrame = currentFrame + 1;
    }

    public String getName() {
        return actorName + "_" + what;
    }

    public AnimatedEntity getEntity() {
        return who;
    }

    public void setRepeat(int count) {
        repeat = count;
    }

    public int getCurrentFrameID() {
        return currentFrame;
    }

    public void setStartFrame(int frameNr) {
        startFrame = frameNr;
        frameCount = endFrame - startFrame;
    }

    public int getStartFrame() {
        return startFrame;
    }

    public void setEndFrame(int frameNr) {
        endFrame = frameNr;
        frameCount = endFrame - startFrame;
    }

    public int getEndFrame() {
        return endFrame;
    }

    public int getTimesPlayed() {
        return timesPlayed;
    }

    public int getFrameCount() {
        return endFrame - startFrame;
    }

    public boolean flipped() {
        return xFlipped || yFlipped;
    }

    public boolean xFlipped() {
        return xFlipped;
    }

    public boolean yFlipped() {
        return yFlipped;
    }

    @Override
    public String toString() {
        String s = who.getName() + "_" + what + "(" + getFrameCount() + "):";
        s += "\n  currentFrame: " + currentFrame;
        s += "\n  nextFrame   : " + nextFrame;
        s += "\n  start       : " + startFrame;
        s += "\n  end         : " + endFrame;
        s += "\n  loop        : " + loop;
        s += "\n  loopBack    : " + loopBack;

        return s;
    }
}