package de.kawaida.kjgame.animation;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.venjinx.core.libgdx.entity.AnimatedEntity;

public class AnimationSet {

    private AnimatedEntity who;

    //    private String actorName;
    private Vector2 size;
    private boolean xFlipped = false;
    private boolean yFlipped = false;

    private Array<AtlasRegion> texArray;
    private HashMap<String, Animation> animations;
    private Animation currentAnimation;
    //    private String returnAnimation;

    public AnimationSet(AnimatedEntity c) {
        who = c;
        //        actorName = c.getName();

        animations = new HashMap<String, Animation>();
        size = new Vector2();
    }

    public boolean update(float deltaT) {
        currentAnimation.update(deltaT);
        return false;
    }

    public Animation setAnimation(String animName) {
        if (!animName.equals(currentAnimation.getName())
                        && animations.containsKey(animName)) {
            currentAnimation = animations.get(animName);
            currentAnimation.reset();
        }
        return currentAnimation;
    }

    public void flip(boolean flipX, boolean flipY) {
        xFlipped = flipX;
        yFlipped = flipY;

        for (AtlasRegion ar : texArray)
            ar.flip(flipX, flipY);
    }

    public void load(TextureAtlas textureAtlas) {
        texArray = textureAtlas.getRegions();

        if (texArray.size == 0) return;

        AtlasRegion frame;
        String animName;
        Animation anim = null;

        for (int i = 0; i < texArray.size; i++) {
            frame = texArray.get(i);

            String[] nameSplit = frame.name.split("#");
            animName = nameSplit[0];

            anim = findFrames(animName, i, texArray);
            i += anim.getFrameCount();

            animations.put(anim.getName(), anim);
        }
        currentAnimation = anim;
    }

    private Animation findFrames(String animName, int start,
                    Array<AtlasRegion> a) {
        AtlasRegion frame;
        Animation anim;

        int count = 0;

        anim = new Animation(who, animName.split("_")[3]);

        while (start + count < a.size) {
            frame = a.get(start + count);
            frame.offsetY = frame.originalHeight - frame.offsetY
                            - frame.getRegionHeight();

            size.x = Math.max(frame.originalWidth, size.x);
            size.y = Math.max(frame.originalHeight, size.y);

            if (frame.name.contains(animName)) {
                count++;
            } else break;
        }

        anim.setStartFrame(start);
        anim.setEndFrame(start + count);
        anim.reset();

        return anim;
    }

    public AtlasRegion getFrame() {
        return texArray.get(currentAnimation.getCurrentFrameID());
    }

    public Animation getAnimation(String name) {
        return animations.get(name);
    }

    public Animation getCurrent() {
        return currentAnimation;
    }

    public HashMap<String, Animation> getAnimations() {
        return animations;
    }

    public void addAnimation(String animName, Animation animation) {
        animations.put(animName, animation);
    }

    public int getAnimCount() {
        return animations.size();
    }

    public Vector2 getSize() {
        return size;
    }

    public boolean flipped() {
        return xFlipped || yFlipped;
    }

    public boolean xFlipped() {
        return xFlipped;
    }

    public boolean yFlipped() {
        return yFlipped;
    }

    public void dispose() {
        currentAnimation = null;
    }
}