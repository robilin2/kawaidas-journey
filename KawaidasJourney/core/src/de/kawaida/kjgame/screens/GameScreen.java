package de.kawaida.kjgame.screens;

import de.kawaida.kjgame.entities.level.GameWorld;
import de.kawaida.kjgame.stages.IngameHUD;
import de.kawaida.kjgame.stages.IntroStage;
import de.kawaida.kjgame.stages.Menu;
import de.venjinx.core.libgdx.Player;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXScreen;

public class GameScreen extends VJXScreen {

    private Player player;

    private Menu menu;
    private GameWorld level;
    private IntroStage intro;
    private IngameHUD ingameHUD;

    public GameScreen(VJXGame game) {
        super(game);

        player = new Player("player", game);

        menu = new Menu(game);
        addStage(menu);

        intro = new IntroStage(game);
        addStage(intro);

        level = new GameWorld(game);
        addStage(level);

        ingameHUD = new IngameHUD(game);
        addStage(ingameHUD);
    }

    public void loadMenu(String name) {
        loader.addToUnloadingQueue(level);
        loader.addToUnloadingQueue(ingameHUD);

        if (name != null && !name.equals("")) menu.setEntryMenu(name);

        loader.addToLoadingQueue(menu);

        game.getView().update((int) game.res.x, (int) game.res.y, true);

        loader.process(false);
    }

    public void loadIntro() {
        loader.addToUnloadingQueue(menu);

        loader.addToLoadingQueue(intro);

        game.getView().update((int) game.res.x, (int) game.res.y, true);

        loader.process(false);
    }

    public void loadLevel(String path, boolean internal) {
        loader.addToUnloadingQueue(intro);

        level.load(path, internal);
        loader.addToLoadingQueue(level);
        loader.addToLoadingQueue(ingameHUD);

        loader.process(false);
    }

    @Override
    public void restart() {
        level.restart();

        game.getScreen().getHUD().updateHUD();

        resume();
    }

    @Override
    public void hide() {
    }

    public Player getPlayer() {
        return player;
    }

    public GameWorld getLevel() {
        return level;
    }

    public IngameHUD getHUD() {
        return ingameHUD;
    }
}