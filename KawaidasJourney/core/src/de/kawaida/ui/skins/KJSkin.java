package de.kawaida.ui.skins;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.venjinx.core.libgdx.VJXFont;

public abstract class KJSkin extends Skin {

    protected Skin defaultWidgets;

    public KJSkin(VJXFont font) {
        setFont(font);
    }

    public KJSkin(ArrayList<TextureAtlas> tas, VJXFont font) {
        this(font);

        defaultWidgets = new Skin(Gdx.files
                        .internal("user_interface/widgets/default/uiskin.json"));

        if (tas != null && !tas.isEmpty())
            for (TextureAtlas ta : tas)
                addRegions(ta);
    }

    public void setFont(VJXFont font) {
        if (has("dflt_vjxFont", VJXFont.class)) {
            remove("dflt_vjxFont", VJXFont.class);
            remove("style_label_solid", LabelStyle.class);
            remove("style_label_outline", LabelStyle.class);
            remove("style_label_shadow", LabelStyle.class);
        }

        add("dflt_vjxFont", font);
        add("style_label_solid", font.getStyleSolid());
        add("style_label_outline", font.getStyleOutline());
        add("style_label_shadow", font.getStyleShadow());

    }

    public Skin getDfltSkin() {
        return defaultWidgets;
    }
}
