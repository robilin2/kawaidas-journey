package de.kawaida.ui.skins;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.venjinx.core.libgdx.VJXFont;
import de.venjinx.core.libgdx.assets.VJXIcon;

public class InterfaceSkin extends KJSkin {

    public InterfaceSkin(ArrayList<TextureAtlas> tas, VJXFont font) {
        super(tas, font);

        Drawable d0 = getDrawable("joy_back");
        Drawable d1 = getDrawable("joy_knob");
        add("style_joystick", new TouchpadStyle(d0, d1));

        ButtonStyle bs;

        d0 = getDrawable("btn_pause_up");
        bs = new ButtonStyle(d0, d1, d0);
        add("style_btn_pause", bs);

        Touchpad tp = new Touchpad(0, this, "style_joystick");
        tp.setName("ui_joystick");
        add("ui_joystick", tp);

        Button button = new Button(this, "style_btn_pause");
        button.setName("ui_btn_pause");
        add("ui_btn_pause", button);

        VJXIcon icon;
        //        icon = new VJXIcon("0", getDrawable("ico_gl_itm_sun"), this, null);
        //        icon.setName("ui_icon_sun");
        //        add("ui_icon_sun", icon);
        //
        //        icon = new VJXIcon("0", getDrawable("ico_gl_itm_totem"), this, null);
        //        icon.setName("ui_icon_totem");
        //        add("ui_icon_totem", icon);
        //
        icon = new VJXIcon("0", getDrawable("ico_gl_itm_cocos"), this, null);
        icon.setName("ui_btn_cocos");
        add("ui_btn_cocos", icon);
    }
}