package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class PauseMenu extends UiTable {

    public PauseMenu(Skin skin) {
        super(skin, "menu_pause");
    }

    @Override
    public void prepare() {
        mapZNZ.setPosition(250, 375);
        mapZNZ.setSize(320, 90);
        addActor(mapZNZ);

        restart.setPosition(650, 375);
        restart.setSize(320, 90);
        addActor(restart);

        settings.setPosition(40, 600);
        settings.setSize(90, 90);
        addActor(settings);

        resume.setPosition(530, 125);
        resume.setSize(200, 150);
        addActor(resume);

        close.setPosition(1160, 615);
        close.setSize(90, 90);
        addActor(close);

    }

}
