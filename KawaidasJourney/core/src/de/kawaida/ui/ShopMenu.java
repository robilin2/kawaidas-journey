package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class ShopMenu extends UiTable {

    public ShopMenu(MenuSkin skin) {
        super(skin, "menu_shop");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);

        shopPrime.setPosition(940, 610);
        shopPrime.setSize(320, 90);
        addActor(shopPrime);
    }

}
