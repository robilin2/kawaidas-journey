package de.kawaida.ui;

import de.kawaida.ui.skins.KJSkin;

public class MapZNZMenu extends UiTable {

    //    private Label selectSkinLabel, selectLevelLabel;
    //    private SelectBox<String> skinSelect, levelSelect;

    public MapZNZMenu(KJSkin skin) {
        super(skin, "menu_map_znz");

    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);

        ency.setPosition(40, 30);
        ency.setSize(300, 90);
        addActor(ency);

        start.setPosition(900, 400);
        start.setSize(90, 90);
        addActor(start);

//        shop.setPosition(1000, 600);
//        shop.setSize(250, 90);
//        addActor(shop);
    }

    //    public String getLevelName() {
    //        return levelSelect.getSelected();
    //    }
}