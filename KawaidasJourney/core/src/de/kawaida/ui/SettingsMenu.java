package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class SettingsMenu extends UiTable {

    public SettingsMenu(MenuSkin skin) {
        super(skin, "menu_settings");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);
        
        music.setPosition(420, 450);
        music.setSize(380, 90);
        addActor(music);
    }

}
