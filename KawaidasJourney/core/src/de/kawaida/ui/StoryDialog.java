package de.kawaida.ui;

import java.util.ArrayDeque;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import de.kawaida.ui.skins.MenuSkin;
import de.kawaida.ui.widgets.KJLabel;
import de.venjinx.core.libgdx.entity.Entity;

public class StoryDialog extends UiTable {

    private ArrayDeque<String> dialogParts;
    private ArrayDeque<SpriteDrawable> dialogSprites;

    private boolean leftSpeaking = false;

    //    private String nextSpeaker;

    private String leftSpeaker;
    private Image leftSpeakerIcon;
    private Cell<Image> leftSpeakerCell;

    private String rightSpeaker;
    private Image rightSpeakerIcon;
    private Cell<Image> rightSpeakerCell;

    private Table dialog;
    private KJLabel currentSpeaker;
    private TextArea text;

    private TextButton nextBtn;

    public StoryDialog(MenuSkin skin) {
        super(skin, "dialog");
        //        dialogParts = new ArrayDeque<>();
        //        dialogParts.add("This is text a cool text area. \nYeay!!!");
        //        dialogParts.add("You are right dude !! This rocks hard !!");

        dialogSprites = new ArrayDeque<>();

        //        dialog = new Table();
        //        dialog.setName("ui_dialog_story");
        //
        //        text = new TextArea(dialogParts.removeFirst(), skin.getDfltSkin());
        //        text.setName("ui_textArea_dialogText");
        //
        //        nextBtn = skin.get("ui_btn_next", TextButton.class);
        //
        //        leftSpeakerIcon = new Image();
        //        leftSpeakerIcon.setName("ui_icon_leftSpeaker");
        //
        //        rightSpeakerIcon = new Image();
        //        leftSpeakerIcon.setName("ui_icon_rightSpeaker");
        //
        //        currentSpeaker = new KJLabel("", skin);
        //        currentSpeaker.setName("ui_label_currentName");
        //
        //        leftSpeakerCell = add(leftSpeakerIcon);
        //
        //        dialog.add(currentSpeaker).top().left();
        //        dialog.add();
        //        dialog.row();
        //        dialog.add(text).width(400).height(150);
        //        dialog.row();
        //        dialog.add(nextBtn).bottom().right();
        //
        //        add(dialog).width(500);
        //
        //        rightSpeakerCell = add(rightSpeakerIcon);
        //        rightSpeakerCell.clearActor();

        //        setFillParent(false);
    }

    public void setDialogParts(ArrayDeque<SpriteDrawable> parts) {
        dialogSprites.addAll(parts);
    }

    public void setSpeakers(Entity left, Entity right) {
        leftSpeaker = left.getName();
        rightSpeaker = right.getName();

        leftSpeakerIcon.setDrawable(left.getAvatarIcon().getDrawable());
        rightSpeakerIcon.setDrawable(right.getAvatarIcon().getDrawable());

        currentSpeaker.setText(leftSpeaker);
        layout();
    }

    //    public void setCurrentSpeaker(String name) {
    //        currentSpeaker.setText(name);
    //    }

    //    public boolean next() {
    //        if (dialogParts.isEmpty()) return false;
    //
    //        leftSpeaking = !leftSpeaking;
    //        if (leftSpeaking) {
    //            rightSpeakerCell.clearActor();
    //            currentSpeaker.setText(leftSpeaker);
    //            leftSpeakerCell.setActor(leftSpeakerIcon);
    //        } else {
    //            leftSpeakerCell.clearActor();
    //            currentSpeaker.setText(rightSpeaker);
    //            rightSpeakerCell.setActor(rightSpeakerIcon);
    //        }
    //        text.clear();
    //        text.setText(dialogParts.removeFirst());
    //
    //        if (!dialogParts.isEmpty()) return true;
    //
    //        return true;
    //    }

    public SpriteDrawable nextSprite() {
        if (dialogSprites.isEmpty()) return null;

        leftSpeaking = !leftSpeaking;
        next.setPosition(leftSpeaking ? 850 : 650, 40);

        //        if (leftSpeaking) {
        //            rightSpeakerCell.clearActor();
        //            currentSpeaker.setText(leftSpeaker);
        //            leftSpeakerCell.setActor(leftSpeakerIcon);
        //        } else {
        //            leftSpeakerCell.clearActor();
        //            currentSpeaker.setText(rightSpeaker);
        //            rightSpeakerCell.setActor(rightSpeakerIcon);
        //        }

        //        if (!dialogSprites.isEmpty()) return null;

        return dialogSprites.removeFirst();
    }

    @Override
    public void prepare() {
    }

}
