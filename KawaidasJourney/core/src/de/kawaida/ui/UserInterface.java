package de.kawaida.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.SnapshotArray;

import de.kawaida.ui.skins.InterfaceSkin;
import de.venjinx.core.libgdx.Player;
import de.venjinx.core.libgdx.assets.VJXIcon;

public class UserInterface extends UiTable {

    private Player player;

    private Touchpad tp;

    private Button pauseButton;

    private Table statusBar;
    private VJXIcon sunsIcon, cocosIcon, totemsIcon;

    public Table bananas;

    public UserInterface(InterfaceSkin skin, String name, Player player) {
        super(skin, name);

        this.player = player;

        tp = skin.get("ui_joystick", Touchpad.class);

        statusBar = new Table();
        statusBar.setName("ui_status_bar");
        statusBar.defaults().pad(25);

        VJXIcon icon;
        sunsIcon = new VJXIcon("0", null, skin, player.getSun().getActor());
        sunsIcon.setName("ui_icon_sun");
        //        add("ui_icon_sun", icon);

        totemsIcon = new VJXIcon("0", null, skin, player.getTotem().getActor());
        totemsIcon.setName("ui_icon_totem");
        //        add("ui_icon_totem", icon);

        //        icon = new VJXIcon("0", getDrawable("ico_gl_itm_cocos"), this, null);
        //        icon.setName("ui_btn_cocos");
        //        add("ui_btn_cocos", icon);

        //        sunsIcon = skin.get("ui_icon_sun", VJXIcon.class);a

        //        statusBar.add(player.getSun().getActor());
        //        statusBar.add(player.getTotem().getActor());

        statusBar.add(sunsIcon);
        statusBar.add(totemsIcon);

        bananas = new Table();
        bananas.setName("ui_bananas");
        Image banana;
        for (int i = 0; i < 4; i++) {
            banana = new Image(skin.getDrawable("ico_gl_itm_banana"));
            banana.setName("ui_icon_banana");
            bananas.add(banana).width(96).height(96);
        }
        statusBar.add(bananas);

        pauseButton = skin.get("ui_btn_pause", Button.class);

        //        cocosIcon = new VJXIcon("0", null, skin, player.getWeapon().getActor());
        //        cocosIcon.setName("ui_icon_cocos");
        cocosIcon = skin.get("ui_btn_cocos", VJXIcon.class);

        clear();

        //        add(player.getSun().getActor());
        //        add(player.getTotem().getActor());
        //        row();

        add(statusBar).expand().top().left();
        add(pauseButton).expand().top().right().pad(25);
        row();
        add();
        add(cocosIcon).expand().bottom().right().width(96).height(96).pad(25);
    }

    public void update() {
        int amount = player.getBanana().getAmount();

        SnapshotArray<Actor> tmpArray = bananas.getChildren();
        for (int i = 0; i < 4; i++) {
            if (i < amount) tmpArray.items[i].setColor(Color.WHITE);
            else tmpArray.items[i].setColor(Color.GRAY);
        }

        sunsIcon.setAmount(player.getSun().getAmount());
        cocosIcon.setAmount(player.getWeapon().getAmount());
        totemsIcon.setAmount(player.getTotem().getAmount());
    }

    @Override
    public void prepare() {

    }

    public Touchpad getJoystick() {
        return tp;
    }

    public void setJoysticPos(float x, float y) {
        tp.setPosition(x - tp.getWidth() / 2, y - tp.getHeight() / 2);
    }

    public boolean paused() {
        return pauseButton.isChecked();
    }
}
