package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class SoundtrackMenu extends UiTable {

    public SoundtrackMenu(Skin skin) {
        super(skin, "menu_soundtrack");
    }

    @Override
    public void prepare() {

    }

}
