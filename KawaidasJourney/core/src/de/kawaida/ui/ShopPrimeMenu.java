package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class ShopPrimeMenu extends UiTable {

    public ShopPrimeMenu(MenuSkin skin) {
        super(skin, "menu_shop_prime");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);

        shop.setPosition(940, 610);
        shop.setSize(320, 90);
        addActor(shop);
    }

}
