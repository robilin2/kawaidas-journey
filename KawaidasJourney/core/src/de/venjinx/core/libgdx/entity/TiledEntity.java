package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class TiledEntity extends NewEntity {

    private Array<Cell> cells;

    public TiledEntity(EntityDef def, BodyType type) {
        super(def, type);
        cells = new Array<>();
    }

    public void setCells(Array<Cell> cells) {
        this.cells.addAll(cells);
    }

    @Override
    public void collideWith(NewEntity other) {
        // TODO Auto-generated method stub

    }

    @Override
    public void trigger(Fixture triggerFixture) {
        // TODO Auto-generated method stub

    }

    @Override
    public void userAct(float deltaT) {
        // TODO Auto-generated method stub

    }

    private Vector2 drawPos = new Vector2();

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        for (Cell c : cells) {
            drawTransform.getTranslation(drawPos);
            drawPos.add(c.getTile().getOffsetX(), c.getTile().getOffsetY());

            batch.draw(c.getTile().getTextureRegion(), drawPos.x, drawPos.y);
        }

    }

    @Override
    protected void createFixtures() {
        MapProperties mp;
        String name, attr;
        float x = 0;

        //        EntityDef def = new EntityDef(mp);
        //
        //        BodyEditorLoader loader = def.getBodyLoader();
        //
        //        Model model = loader.getInternalModel();
        //
        //        HashMap<String, RigidBodyModel> map = (HashMap<String, RigidBodyModel>) model.rigidBodies;
        //
        //        name = "tile_" + def.getUniqueName() + "_" + def.getAttributes()
        //                        + ".png";
        //
        //        RigidBodyModel rbModel = map.get(name);

        //                    System.out.println();
        //                    Vector2 offset = new Vector2();
        //                    for (PolygonModel pm : rbModel.polygons) {
        //                        for (Vector2 v : pm.vertices) {
        //                            offset.set(n, m);
        //                            System.out.println("off " + offset);
        //                            System.out.println("v" + v);
        //                            v.add(offset);
        //                            System.out.println(v);
        //                        }
        //                    }

        //        rbModel.origin.set(-n, -m);

        FixtureDef fd = new FixtureDef();
        fd.friction = 0;
        fd.restitution = 0;
        fd.filter.categoryBits = b2dCategory;
        fd.filter.maskBits = b2dMask;
        //                    fd.filter.maskBits = VJXUtility.PhysCat.NONE.id;dddd

        //        loader.attachFixture(body, name, fd, 1.28f);

        Array<Fixture> fList = body.getFixtureList();
        Fixture f = fList.peek();
        //        f.setUserData(new VJXData(def.getWho(), this));

        for (Cell c : cells) {
            mp = c.getTile().getProperties();
            name = mp.get("name", String.class);
            //                            env = Environment.get(mp.get("environment", String.class));
            //                            cat = Category.get(mp.get("category", String.class));
            attr = mp.get("attributes", String.class);

            if (attr.contains("mid")) {
                x += 128;
                //                                    vertices.add(new Vector2(
                //                                                    tmpN * getTileWidth() + 64,
                //                                                    m * getTileHeight() + 100));
                //                                    vertices.add(new Vector2(
                //                                                    tmpN++ * getTileWidth() + 128,
                //                                                    m * getTileHeight() + 100));

            }
            if (attr.contains("right")) {
                x += 90;
                //                                    vertices.add(new Vector2(
                //                                                    tmpN * getTileWidth() + 64,
                //                                                    m * getTileHeight() + 100));
                //                                    vertices.add(new Vector2(
                //                                                    tmpN++ * getTileWidth() + 90,
                //                                                    m * getTileHeight() + 100));

            }
        }
    }

    @Override
    protected void updateFixtures() {
        // TODO Auto-generated method stub

    }

    @Override
    public void spawnActor(World world) {
        // TODO Auto-generated method stub

    }

}
