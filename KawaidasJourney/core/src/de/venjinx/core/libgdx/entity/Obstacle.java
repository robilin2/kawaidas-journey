package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;

public class Obstacle extends Terrain {

    public Obstacle(EntityDef def) {
        super(0, 0, def);
    }

    MoveByAction ma = new MoveByAction();

    public Obstacle(float width, float groundLvl, EntityDef def) {
        super(width, groundLvl, def);
        type = BodyType.KinematicBody;
        actor.setName("gl_trn_obstacle");

        //        ma.setReverse(false);
        ma.setTarget(actor);
        ma.setAmount(10, 0);
        ma.setDuration(5);
        ma.setTime(0);
        ma.reset();
        actor.addAction(ma);
    }

    //    public Obstacle(String who, BodyType type) {
    //        super();
    //        setName("gl_trn_obstacle");
    //
    //        chainVerts = new LinkedList<>();
    //    }

    @Override
    protected void createFixtures() {
        //        body.destroyFixture(boundFixture);
        //        rootShape = new ChainShape();
        //
        //        //        if (chainVerts.size() == 0 && lvlWidth != 0) {
        //        //            chainVerts.add(new Vector2(0, groundLvl + 2000).scl(1 / 100f));
        //        //            chainVerts.add(new Vector2(0, groundLvl).scl(1 / 100f));
        //        //            chainVerts.add(new Vector2(lvlWidth, groundLvl).scl(1 / 100f));
        //        //            chainVerts.add(new Vector2(lvlWidth, groundLvl + 2000)
        //        //                            .scl(1 / 100f));
        //        //        }
        //
        //        //        if (chainVerts.size() > 0) {
        //        //            rootShape.createChain(
        //        //                            chainVerts.toArray(new Vector2[chainVerts.size()]));
        //        //
        //        FixtureDef fixDef = new FixtureDef();
        //        fixDef.shape = rootShape;
        //        fixDef.filter.categoryBits = b2dCategory;
        //        fixDef.filter.maskBits = b2dMask;
        //        fixDef.restitution = 0f;
        //        fixDef.friction = 0f;
        //        //
        //        boundFixture = body.createFixture(fixDef);
        //        boundFixture.setUserData(new VJXData("bound", this));
        //        //
        //        rootShape = (ChainShape) boundFixture.getShape();
        //        //        }
        //        boundShape = rootShape;
    }

}