package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.kawaida.kjgame.animation.Animation;
import de.kawaida.kjgame.animation.AnimationSet;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;

public abstract class AnimatedEntity extends NewEntity {

    private FixtureDef headDef;
    private FixtureDef feetDef;
    private Fixture headFixture;
    private Fixture feetFixture;
    protected AnimationSet animations;

    public AnimatedEntity(EntityDef def, BodyType type) {
        super(def, type);

        b2dCategory = PhysCat.CHARACTER.id;
        b2dMask = (short) (PhysCat.NO_PASS.id | PhysCat.TRIGGER.id);

        headDef = new FixtureDef();
        headDef.restitution = 0f;
        headDef.friction = 0f;
        headDef.isSensor = true;

        feetDef = new FixtureDef();
        feetDef.restitution = 0f;
        feetDef.friction = 0f;
        feetDef.isSensor = true;
    }

    public void setAnimations(AnimationSet anims) {
        animations = anims;
        animations.setAnimation(actor.getName() + "_" + status.name);
        setFrame(animations.getFrame());


        //        if (frame != null && actor.getStage() != null)
        //            resizeBound(anim.getSize().x / 1.5f, anim.getSize().y / 1.5f,
        //                            anim.getOffset().x, anim.getOffset().y);

        setSize(animations.getSize());
        actor.setSize(size.x, size.y);

        //        updateDrawTransform();
    }

    @Override
    public void act(float deltaT) {
        //        if (status.blocking)
        if (statusChanged()) {
            animations.setAnimation(actor.getName() + "_" + status.name);
            setUnchanged();
        }

        animations.update(deltaT);
        setFrame(animations.getFrame());

        super.act(deltaT);
    }

    @Override
    protected void createFixtures() {
        if (getName().contains("kawaida")) return;
        Animation anim = animations.getCurrent();

        Vector2 size = new Vector2(actor.getWidth(), actor.getHeight());

        //        size.scl(.75f);

        PolygonShape feetShape, headShape;

        float halfW, halfH;
        Vector2 center = new Vector2();

        halfW = size.x / 200f;
        halfH = size.y / 200f;

        center.add(halfW, 0);

        feetShape = new PolygonShape();
        feetShape.setAsBox(halfW / 2f, .05f, center, 0);

        feetDef.shape = feetShape;
        feetDef.filter.categoryBits = PhysCat.CHARACTER.id;
        feetDef.filter.maskBits = PhysCat.NO_PASS.id;

        feetFixture = body.createFixture(feetDef);
        feetFixture.setUserData(new VJXData(actor.getName() + "_feet", this));

        feetShape = (PolygonShape) feetFixture.getShape();

        center.set(0, 0);
        center.add(halfW, halfH);

        headShape = new PolygonShape();
        headShape.setAsBox(halfW / 2f, .05f, center, 0);

        headDef.shape = headShape;
        headDef.filter.categoryBits = (short) (PhysCat.CHARACTER.id
                        | PhysCat.TRIGGER.id);
        headDef.filter.maskBits = (short) (PhysCat.CHARACTER.id
                        | PhysCat.NO_PASS.id);

        headFixture = body.createFixture(headDef);
        headFixture.setUserData(new VJXData(actor.getName() + "_head", this));

        headShape = (PolygonShape) headFixture.getShape();
    }

    @Override
    protected void updateFixtures() {
        Vector2 size = new Vector2(frame.getRegionWidth(),
                        frame.getRegionHeight());

        PolygonShape feetShape, headShape;

        float halfW, halfH, offX, offY;
        Vector2 center = new Vector2();

        //        if (getName().contains("kawaida"))
        //        System.out.println(size);

        halfW = size.x / 200f;
        halfH = size.y / 200f;

        offX = frame.offsetX * .01f;
        offY = frame.offsetY * .01f;

        center.add(offX + halfW, offY);
        center.add(0, .05f);

        feetShape = new PolygonShape();
        feetShape.setAsBox(halfW / 2f, .05f, center, 0);

        feetDef.shape = feetShape;
        feetDef.filter.categoryBits = PhysCat.CHARACTER.id;
        feetDef.filter.maskBits = PhysCat.NO_PASS.id;

        feetFixture = body.createFixture(feetDef);
        feetFixture.setUserData(new VJXData(actor.getName() + "_feet", this));

        feetShape = (PolygonShape) feetFixture.getShape();

        center.set(0, 0);
        center.add(offX + halfW, offY + size.y * .01f);
        center.sub(0, .05f);

        headShape = new PolygonShape();
        headShape.setAsBox(halfW / 2f, .05f, center, 0);

        headDef.shape = headShape;
        headDef.filter.categoryBits = (short) (PhysCat.CHARACTER.id
                        | PhysCat.TRIGGER.id);
        headDef.filter.maskBits = (short) (PhysCat.CHARACTER.id
                        | PhysCat.NO_PASS.id);

        headFixture = body.createFixture(headDef);
        headFixture.setUserData(new VJXData(actor.getName() + "_head", this));

        headShape = (PolygonShape) headFixture.getShape();
    }

    public AnimationSet getAnimations() {
        return animations;
    }
}