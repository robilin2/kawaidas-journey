package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.scenes.scene2d.Group;

public class EntityGroup extends Group {

    public EntityGroup(String name) {
        setName(name);
    }

    //    @Override
    //    public boolean removeActor(Actor actor, boolean unfocus) {
    //        super.removeActor(actor, unfocus);
    //
    //        if (actor instanceof VJXEntity) if (actor.getStage() instanceof KJLevel)
    //            ((KJLevel) actor.getStage()).getB2DWorld()
    //                            .destroyBody(((VJXEntity) actor).getBody());
    //        return true;
    //    }
}