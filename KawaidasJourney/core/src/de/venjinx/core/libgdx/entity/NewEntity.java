package de.venjinx.core.libgdx.entity;

import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.level.GameWorld;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.kawaida.kjgame.interfaces.Visitor;
import de.venjinx.core.libgdx.BodyEditorLoader.Model;
import de.venjinx.core.libgdx.BodyEditorLoader.RigidBodyModel;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.StatusChangedEvent;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;
import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.assets.AvatarIcon;

public abstract class NewEntity implements Acceptor {

    protected final static HashSet<AssetDef<? extends Object>> resources = new HashSet<>();

    // unique identifier managed by VJXGame.class
    private long id = -1;

    // indicates that the entity changed its activity
    private boolean statusChanged = false;

    // entity game status: spawned > alive -> always in in world
    protected boolean spawned = false; //  not in world->spawn->in world->despawn
    protected boolean alive = false; // spawn->alive->die->not alive->respawn

    // physics category and collision mask
    protected short b2dCategory = 0;
    protected short b2dMask = 0;

    // entity world status
    protected Status status = Status.IDLE;

    // the direction entity is facing and moving
    protected VJXDirection direction = VJXDirection.Right;

    // if 0 is not touching any ground
    protected int onGround = 0;

    // the entities icon and current actively drawn image
    protected AvatarIcon icon;
    protected AtlasRegion frame;

    protected Actor actor;

    // the entities bounding box definition and shape
    protected FixtureDef boundDef;
    protected Fixture boundFixture;
    protected Shape boundShape;

    // the entities physical body
    protected BodyType type;
    protected Body body;

    protected EntityDef definition;

    protected Vector2 size = new Vector2();

    protected Vector2 position = new Vector2();
    protected Affine2 posTransform = new Affine2();
    // transform for drawing the current image at the correct location
    // relative to the entities center-based physical position
    // body position -> draw position (bottom-left -> up-right)
    protected Affine2 drawTransform = new Affine2();

    public NewEntity(EntityDef def, BodyType type) {
        this.type = type;
        definition = def;
        id = VJXGame.newID();

        actor = new Actor() {
            @Override
            public void positionChanged() {
                position.set(this.getX(), this.getY());
                if (updateBody)
                    body.setTransform(new Vector2(position).scl(.01f),
                                body.getAngle());

                updateDrawTransform();
            }

            @Override
            public void draw(Batch batch, float parentAlpha) {
                if (frame != null) batch.draw(frame, frame.getRegionWidth(),
                                frame.getRegionHeight(), drawTransform);
                userDraw(batch, parentAlpha);
            }
        };

        actor.setName(def.getUniqueName());

        boundDef = new FixtureDef();
        boundDef.restitution = 0f;
        boundDef.friction = 0f;
    }

    //    public Entity(String who, BodyType type) {
    //        this.type = type;
    //        id = VJXGame.newID();
    //        setName(who);
    //
    //        boundDef = new FixtureDef();
    //        boundDef.restitution = 0f;
    //        boundDef.friction = 0f;
    //    }

    public abstract void collideWith(NewEntity other);

    public abstract void trigger(Fixture triggerFixture);

    public abstract void userAct(float deltaT);

    public abstract void userDraw(Batch batch, float parentAlpha);

    protected abstract void createFixtures();

    protected abstract void updateFixtures();

    public abstract void spawnActor(World world);

    public void act(float deltaT) {
        updateBody = body != null;

        Vector2 newPos = new Vector2();

        if (updateBody) {
            newPos.set(body.getPosition()).scl(100f);

            updateBody = false;

            actor.setPosition(newPos.x, newPos.y);
            updateBody = true;
        }

        userAct(deltaT);

        updateDrawTransform();
    }

    private boolean updateBody = false;

    //    public void draw(Batch batch, float parentAlpha) {
    //        if (frame != null) batch.draw(frame, frame.getRegionWidth(),
    //                        frame.getRegionHeight(), drawTransform);
    //        userDraw(batch, parentAlpha);
    //    }

    @Override
    public void accVisitor(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accCollision(Visitor visitor) {
        visitor.hit(this);
    }

    @Override
    public void accTrigger(Visitor visitor) {
        visitor.trigger(this);
    }

    public void setSize(Vector2 newSize) {
        setSize(newSize.x, newSize.y);
    }

    public void setSize(float width, float height) {
        size.set(width, height);
        //        actor.setOrigin(0, 0);
        //        actor.setOrigin(width / 2f, height / 2f);
    }

    public Body spawn(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;

        body = world.createBody(bodyDef);
        body.setSleepingAllowed(false);
        body.setUserData(new VJXData(actor.getName(), this));
        body.setTransform(position.x * .01f, position.y * .01f, 0);

        //        setStatus(VJXStatus.SPAWN, true);
        setStatus(Status.IDLE, true);

        // create entity hit boxes
        createBound();

        // create custom fixtures from classes extending this class
        createFixtures();

        updateBody();

        spawned = true;
        alive = true;
        updateBody = true;

        return body;
    }

    public void despawn() {
        spawned = false;
        alive = false;
    }

    public long getID() {
        return id;
    }

    public void setStatus(Status status) {
        setStatus(status, false);
    }

    StatusChangedEvent statusEvent = new StatusChangedEvent();

    public void setStatus(Status status, boolean force) {
        if (this.status == status) return;
        if (this.status.blocking && !force) return;

        statusEvent.reset();
        statusEvent.oldStatus = this.status;
        statusEvent.newStatus = status;
        statusEvent.setTarget(actor);

        actor.notify(statusEvent, false);

        if (status == Status.DIE) {
            alive = false;
            body.setLinearVelocity(0, 0);

            for (Fixture f : body.getFixtureList())
                if (this instanceof Enemy) {
                    b2dCategory = PhysCat.CHARACTER.id;
                    b2dMask = PhysCat.NO_PASS.id;

                    Filter filter = new Filter();
                    filter.categoryBits = b2dCategory;
                    filter.maskBits = b2dMask;
                    f.setFilterData(filter);
                }
        }

        this.status = status;
        statusChanged = true;
    }

    public boolean statusChanged() {
        return statusChanged;
    }

    public void setUnchanged() {
        statusChanged = false;
    }

    public Status getStatus() {
        return status;
    }

    public void setAvatarIcon(AvatarIcon icon) {
        this.icon = icon;
    }

    public AvatarIcon getAvatarIcon() {
        return icon;
    }

    public void setFrame(AtlasRegion f) {
        frame = f;
        //        if (frame != null) {
        //            actor.setSize(frame.getRegionWidth(), frame.getRegionHeight());
        //        }
    }

    public void setDirection(VJXDirection direction) {
        this.direction = direction;
    }

    public void resetOnGround() {
        onGround = 0;
    }

    public void incrOnGround() {
        onGround++;
    }

    public void decrOnGround() {
        onGround = onGround > 0 ? onGround - 1 : onGround;
    }

    public boolean isOnGround() {
        return onGround > 0;
    }

    public void setCategory(short category) {
        b2dCategory = category;

        updateFilter();
    }

    public short getB2DCategory() {
        return b2dCategory;
    }

    public void setCollisionMask(short mask) {
        b2dMask = mask;

        updateFilter();
    }

    public void setSensor(boolean sensor) {
        for (Fixture f : body.getFixtureList())
            f.setSensor(sensor);
    }

    public short getCollisionMask() {
        return b2dMask;
    }

    public void setPhysics(short category, short mask) {
        setCategory(category);
        setCollisionMask(mask);
    }

    private void updateFilter() {
        for (Fixture f : body.getFixtureList())
            if (!f.isSensor()) {
                Filter filter = new Filter();
                filter.categoryBits = b2dCategory;
                filter.maskBits = b2dMask;
                f.setFilterData(filter);
            }
    }

    public Body getBody() {
        return body;
    }

    public Actor getActor() {
        return actor;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
        actor.setPosition(x, y);

        updateDrawTransform();

        //        if (getName().contains("kawaida")) {
        //            System.out.println("Set position: " + x + ", " + y);
        //            System.out.println("Set actor position: "
        //                            + (x - actor.getWidth() / 2f) + ", "
        //                            + (y - actor.getHeight() / 2f));
        //            System.out.println(getInfo());
        //            System.out.println();
        //            System.out.println("--------------------------");
        //            System.out.println();
        //        }
    };

    public void setPosition(Vector2 pos) {
        setPosition(pos.x, pos.y);
    }

    public Affine2 getTransform() {
        return posTransform;
    }

    public float getDrawX() {
        if (frame == null) return 0f;
        return actor.getX() + frame.offsetX;
    }

    public float getDrawY() {
        if (frame == null) return 0f;
        return actor.getY() + frame.offsetY;
    }

    //    public Vector2 getDrawPosition() {
    //        if (frame == null) return new Vector2(actor.getX(), actor.getY());
    //
    //        return new Vector2(actor.getX() + frame.offsetX, actor.getY() + frame.offsetY);
    //    }

    public Vector2 getCenter() {
        return new Vector2(actor.getX() + actor.getWidth() / 2f,
                        actor.getY() + actor.getHeight() / 2f);
    }

    public GameWorld getWorld() {
        return (GameWorld) actor.getStage();
    }

    public VJXDirection getFaceDirection() {
        return direction;
    }

    public boolean isSpawned() {
        return spawned;
    }

    public boolean isAlive() {
        return alive;
    }

    protected void updateBody() {
        if (!getName().contains("kawaida")) return;

        Array<Fixture> fixtures = body.getFixtureList();
        for (Fixture f : fixtures) {
            body.destroyFixture(f);

        }
        boundDef.filter.categoryBits = b2dCategory;
        boundDef.filter.maskBits = b2dMask;

        Model model = definition.getBodyLoader().getInternalModel();

        HashMap<String, RigidBodyModel> map =
                        (HashMap<String, RigidBodyModel>) model.rigidBodies;

        String name = frame.name.split("#")[0];

        //        System.out.println("load " + name);

        RigidBodyModel rbModel;
        for (String s : map.keySet()) {
            rbModel = map.get(s);
            //            System.out.println("key: " + s + ", value: " + rbModel.name);
            //            for (Vector2 v : pm.vertices) {
            //                //                offset.set(n, m);
            //                System.out.println("off " + offset);
            //                System.out.println("v" + v);
            //                //                v.add(offset);
            //                //                System.out.println(v);
            //            }
        }

        rbModel = map.get(name);

        Vector2 offset = new Vector2();
        //        for (PolygonModel pm : rbModel.polygons) {
        //            System.out.println(pm);
            //            for (Vector2 v : pm.vertices) {
            //                offset.set(body.getPosition());
            //                System.out.println("off " + offset);
            //                System.out.println("v" + v);
            //                v.add(offset);
            //                //                System.out.println(v);
            //            }
        //        }

        //        System.out.println(getInfo());

        //        rbModel.origin.set(-body.getPosition().x, -body.getPosition().y);
        //        rbModel.origin.set(body.getLocalCenter());

        definition.getBodyLoader().attachFixture(body, name, boundDef,
                        size.x * .01f, size.y * .01f, 1f);
        updateFixtures();
    }

    //    protected void resizeBound() {
    //        resizeBound(frame);
    //    }
    //
    //    protected void resizeBound(AtlasRegion frame) {
    //        if (frame != null) resizeBound(frame.getRegionWidth(),
    //                        frame.getRegionHeight(), frame.offsetX, frame.offsetY);
    //    }
    //
    //    protected void resizeBound(float w, float h, float offX, float offY) {
    //        if (boundShape != null) {
    //            if (boundShape instanceof PolygonShape) {
    //                ((PolygonShape) boundShape).setAsBox(w / 200f, h / 200f,
    //                                new Vector2(actor.getWidth() / 200f,
    //                                                actor.getHeight() / 200f), 0);
    //                return;
    //            }
    //
    //            if (boundShape instanceof ChainShape) {
    //                //                ((ChainShape) boundShape).createChain(new Vector2[] {
    //                //                                new Vector2(0, offY / 100f),
    //                //                                new Vector2(w / 100f, offY / 100f) });
    //                return;
    //            }
    //        }
    //    }

    protected void updateDrawTransform() {
        if (frame == null) return;
        // update the draw transform, draw pos is bottom left corner of object

        // load identity
        drawTransform.idt();

        // translate to entity position
        //        if (actor.getParent() != null)
        //            drawTransform.translate(actor.getParent().getX(),
        //                            actor.getParent().getY());

        drawTransform.translate(actor.localToStageCoordinates(new Vector2()));
        //        drawTransform.translate(actor.getX(), actor.getY());

        // translate to origin
        //        drawTransform.translate(-frame.originalWidth / 2f,
        //                        -frame.originalHeight / 2f);

        // translate to draw
        drawTransform.translate(frame.offsetX, frame.offsetY);

        // mirror the image if object is facing left
        if (direction == VJXDirection.Left) {
            drawTransform.translate(frame.getRegionWidth(), 0f);
            drawTransform.scale(-1, 1);
        }
    }

    private void createBound() {
        if (getName().contains("kawaida")) return;
        float halfW, halfH;

        halfW = actor.getWidth() / 200f;
        halfH = actor.getHeight() / 200f;

        boundShape = new PolygonShape();
        ((PolygonShape) boundShape).setAsBox(halfW, halfH,
                        new Vector2(halfW, halfH), 0);

        boundDef.shape = boundShape;
        boundDef.filter.categoryBits = b2dCategory;
        boundDef.filter.maskBits = b2dMask;

        boundFixture = body.createFixture(boundDef);
        boundFixture.setUserData(new VJXData(getName() + "_bound", this));

        boundShape = boundFixture.getShape();
    }

    public String getInfo() {
        String s = toString();

        s += "\n  BodyType   : " + type;
        s += "\n  Status     : " + status;
        s += "\n  Face       : " + direction;
        s += "\n  Category   : " + b2dCategory;
        s += "\n  Size       : [" + actor.getWidth() + ":" + actor.getHeight() + "]";
        s += "\n  Position   : [" + actor.getX() + ":" + actor.getY() + "]";
        s += "\n  DrawPos    : \n" + drawTransform;
        s += "\n  Center     : " + getCenter();
        s += "\n  Origin     : [" + actor.getOriginX() + ":" + actor.getOriginY() + "]";
        s += "\n  UserData   : " + actor.getUserObject();
        s += "\n  Body       : " + body;
        if (body != null) {
            s += "\n  BodyCenter : " + body.getLocalCenter();
            s += "\n  BodyPos    : " + body.getPosition();
            s += "\n  BodyTrans  : " + body.getTransform().getPosition();
            s += "\n  BodyWCenter: " + body.getWorldCenter();
        }

        s += "\n";

        return s;
    }

    public String getName() {
        return definition.getUniqueName();
    }
}