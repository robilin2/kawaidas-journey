package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.scenes.scene2d.Event;

public class EntityEvent extends Event {

    public String name = "";

    public EntityEvent(Entity src, String name) {
        this(src, src, name);
        this.name = name;
    }

    public EntityEvent(Entity src, Entity target, String name) {
        setListenerActor(src);
        setTarget(target);
        setBubbles(false);
        setStage(src.getStage());
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}