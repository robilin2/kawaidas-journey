package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;

import de.kawaida.kjgame.animation.Animation;
import de.kawaida.kjgame.animation.Animation.AnimationEvent;
import de.kawaida.kjgame.entities.actors.Kawaida;
import de.kawaida.kjgame.entities.enemies.Bird;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXUtility;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.StatusChangedEvent;

public class EntityListener implements EventListener {

    private VJXGame game;

    public EntityListener(VJXGame game) {
        this.game = game;
    }

    @Override
    public boolean handle(Event event) {
        Actor entity = event.getListenerActor();
        String eName = entity.getName();

        //        System.out.println(eName);
        //        Actor target = event.getTarget();

        if (event instanceof StatusChangedEvent) {
            StatusChangedEvent se = (StatusChangedEvent) event;

            //            System.out.print("status changed: " + entity.getName());
            //            System.out.print(" --> " + se.getName());
            //            System.out.println(" playSound: fx_" + entity.getName() + "_"
            //                            + se.status.name);

            String sfxName = "sfx_" + entity.getName();
            if (se.newStatus != Status.IDLE)
                game.sfx.play(sfxName + "_" + se.newStatus.name,
                                se.newStatus.activity);
            game.sfx.stopSound(sfxName + "_" + se.oldStatus.name);
        }

        if (event instanceof AnimationEvent) {
            AnimationEvent ae = (AnimationEvent) event;
            AnimatedEntity aEntity = ae.getAnimation().getEntity();
            Animation anim = ae.getAnimation();

            if (eName.contains("palm")) {
                if (ae.getName().contains("finished")) {
                    game.getScreen().getHUD().showProject();
                    game.getScreen().getLevel().updateEvoLevel();
                    aEntity.setStatus(Status.ACTIVE);
                }
                return true;
            }

            if (eName.contains("kawaida")) {
                Kawaida kAvatar = (Kawaida) ae.animation.getEntity();

                if (ae.getName().contains("finished")) {
                    anim.reset();

                    if (anim.getName().contains("hit")) {
                        kAvatar.decrHealth();
                        if (game.getPlayer().getBanana().getAmount() > 0) {
                            game.getPlayer().getBanana().decr();
                            game.getScreen().getHUD().updateHUD();
                            kAvatar.incrHealth();
                        }

                        if (kAvatar.getHealth() == 0)
                            kAvatar.setStatus(Status.DIE, true);
                        else
                            kAvatar.setStatus(Status.IDLE, true);
                        return true;
                    }

                    if (anim.getName().contains("throw")) {
                        kAvatar.setStatus(Status.IDLE, true);
                        return true;
                    }

                    if (anim.getName().contains("die")) {
                        //                        game.sfx.stopAllSounds();
                        game.getScreen().getHUD().lose();
                        return true;
                    }
                }
                return true;
            }

            if (eName.contains("bird")) {
                if (ae.getName().contains("fly"))
                    ((Bird) aEntity).flap();
                return true;
            }
        }
        return false;
    }

}
