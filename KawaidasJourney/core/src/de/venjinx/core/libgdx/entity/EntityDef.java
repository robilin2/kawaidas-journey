package de.venjinx.core.libgdx.entity;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.core.libgdx.BodyEditorLoader;
import de.venjinx.core.libgdx.BodyEditorLoader.Model;
import de.venjinx.core.libgdx.BodyEditorLoader.RigidBodyModel;
import de.venjinx.core.libgdx.VJXUtility.Category;
import de.venjinx.core.libgdx.VJXUtility.Environment;
import de.venjinx.core.libgdx.VJXUtility.Status;

public class EntityDef {

    private String who;
    private String uName;
    private String path;

    private MapProperties properties;
    private Environment environment;
    private Category category;

    private BodyEditorLoader bodyLoader;
    private HashMap<String, AssetDescriptor<? extends Object>> assetDefs;

    public EntityDef() {
        properties = new MapProperties();
        assetDefs = new HashMap<>();

        parseUniqueName("dev_stg_dummy");
    }

    public void setFromProps(MapProperties entityProps) {
        properties = entityProps;

        environment = Environment
                        .get(properties.get("environment", String.class));
        category = Category.get(properties.get("category", String.class));

        who = properties.get("name", String.class);
        uName = environment.envShort + "_" + category.catShort + "_" + who;

        String s[] = getAttributes().split("_");
        String tmpPath;

        // set correct internal entity path, where all its assets are located
        if (s.length == 0) tmpPath = environment.envName + "/"
                        + category.catName + "/" + who + "/";
        else {
            tmpPath = environment.envName + "/" + category.catName + "/";
            for (String str : s) {
                if (!str.equals("")) {
                    tmpPath += str + "/";
                }
            }
            tmpPath += who + "/";
        }

        assetDefs = new HashMap<>();

        // entity animations
        assetDefs.put("anims_" + uName,
                        new AssetDescriptor<>(
                                        tmpPath + "anims_" + uName + ".atlas",
                                        TextureAtlas.class));

        path = tmpPath;

        // entity sounds
        tmpPath += "sfx/";

        String fileName;
        // add action sounds
        if (getActions() != null) {
            s = getActions().split(",");
            if (s.length > 0 && !s[0].isEmpty()) {
                for (String str : s) {
                    fileName = "sfx_" + uName + "_" + Status.get(str).name;
                    assetDefs.put(fileName, new AssetDescriptor<>(
                                    tmpPath + fileName + ".mp3", Sound.class));
                }
            }
        }

        // add activity sounds
        if (getActivities() != null) {
            s = getActivities().split(",");
            if (s.length > 0 && !s[0].isEmpty()) {
                for (String str : s) {
                    fileName = "sfx_" + uName + "_" + Status.get(str).name;
                    assetDefs.put(fileName, new AssetDescriptor<>(
                                    tmpPath + fileName + ".mp3", Sound.class));
                }
            }
        }
    }

    public EntityDef parseUniqueName(String uName) {
        if (uName == null || uName.isEmpty()) return null;

        String[] str = uName.split("_");
        if (str.length < 3) return null;

        properties.put("environment", Environment.get(str[0]).envName);
        properties.put("category", Category.get(str[1]).catName);
        properties.put("name", "dev");
        properties.put("attributes", "");
        properties.put("actions", "");
        properties.put("activities", "");

        if (str.length > 3) properties.put("attributes", str[3]);

        return this;
    }

    public EntityDef(MapProperties entityProps) {
        properties = entityProps;

        environment = Environment.get(properties.get("environment", String.class));
        category = Category.get(properties.get("category", String.class));

        who = properties.get("name", String.class);
        uName = environment.envShort + "_" + category.catShort + "_" + who;

        String s[] = getSubcategory().split("_");
        String tmpPath;

        // set correct internal entity path, where all its assets are located
        if (s.length == 0)
            tmpPath = environment.envName + "/"
                            + category.catName + "/"
                            + who + "/";
        else {
            tmpPath = environment.envName + "/" + category.catName + "/";
            for (String str : s) {
                if (!str.equals("")) {
                    tmpPath += str + "/";
                }
            }
            tmpPath += who + "/";
        }

        assetDefs = new HashMap<>();

        if (getSubcategory().contains("utility")) return;

        // entity animations
        assetDefs.put("anims_" + uName,
                        new AssetDescriptor<>(
                        tmpPath + "anims_" + uName + ".atlas", TextureAtlas.class));

        path = tmpPath;

        // entity sounds
        tmpPath += "sfx/";

        String fileName;
        // add action sounds
        if (getActions() != null) {
            s = getActions().split(",");
            if (s.length > 0 && !s[0].isEmpty()) {
                for (String str : s) {
                    fileName = "sfx_" + uName + "_" + Status.get(str).name;
                    assetDefs.put(fileName, new AssetDescriptor<>(
                                    tmpPath + fileName + ".mp3", Sound.class));
                }
            }
        }

        // add activity sounds
        if (getActivities() != null) {
            s = getActivities().split(",");
            if (s.length > 0 && !s[0].isEmpty()) {
                for (String str : s) {
                    fileName = "sfx_" + uName + "_" + Status.get(str).name;
                    assetDefs.put(fileName, new AssetDescriptor<>(
                                    tmpPath + fileName + ".mp3", Sound.class));
                }
            }
        }
    }

    public String getWho() {
        return who;
    }

    public String getUniqueName() {
        return uName;
    }

    public String getAttributes() {
        return properties.get("attributes", String.class);
    }

    public String getActions() {
        return properties.get("actions", String.class);
    }

    public String getActivities() {
        return properties.get("activities", String.class);
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Category getCategory() {
        return category;
    }

    public String getSubcategory() {
        return properties.get("subcategory", String.class);
    }

    HashMap<String, RigidBodyModel> rigidBody;

    public BodyEditorLoader loadBodyDef() {
        bodyLoader = null;

        FileHandle file = Gdx.files.internal(path + "bodies_" + uName + ".def");
        if (file.exists()) {
            bodyLoader = new BodyEditorLoader(file);
            Model m = bodyLoader.getInternalModel();

            rigidBody = (HashMap<String, RigidBodyModel>) m.rigidBodies;

            //            for (RigidBodyModel rbm : rigidBody.values()) {
            //                if (rbm.name.contains("kawaida")) {
            //                    System.out.println(rbm.name);
            //                    System.out.println(rbm.imagePath);
            //                    System.out.println(rbm.polygons.size());
            //                    System.out.println("polygons:");
            //                    for (PolygonModel pm : rbm.polygons) {
            //                        System.out.println(pm.vertices);
            //                    }
            //                    System.out.println("----------------");
            //                }
            //            }
        }
        return bodyLoader;
    }

    public BodyEditorLoader getBodyLoader() {
        return bodyLoader;
    }

    public HashMap<String, AssetDescriptor<? extends Object>> getAssetDefs() {
        return assetDefs;
    }

    public String getPath() {
        return path;
    }

    public int getID() {
        return properties.get("gid", Integer.class);
    }

    @Override
    public String toString() {
        String str = "";
        str += "EntityDef: " + path + "\n";
        str += "who: " + who + "\n";
        str += "uName: " + uName + "\n";
        str += "environment: " + environment.envName + " - " + environment.envShort + "\n";
        str += "category: " + category.catName + " - " + category.catShort + "\n";
        str += "attributes: " + getAttributes() + "\n";

        return str;
    }

    public static EntityDef fromUniqueName(String uName) {
        MapProperties entityProps = new MapProperties();

        if (uName == null || uName.isEmpty()) return null;

        String[] str = uName.split("_");
        if (str.length < 3) return null;

        entityProps.put("environment", Environment.get(str[0]).envName);
        entityProps.put("category", Category.get(str[1]).catName);
        entityProps.put("subcategory", "");
        entityProps.put("name", str[2]);
        entityProps.put("attributes", "");
        entityProps.put("actions", "");
        entityProps.put("activities", "");

        if (str.length > 3) entityProps.put("attributes", str[3]);

        return new EntityDef(entityProps);
    }
}