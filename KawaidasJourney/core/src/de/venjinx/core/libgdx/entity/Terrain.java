package de.venjinx.core.libgdx.entity;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXUtility.PhysCat;

public class Terrain extends NewEntity {

    private float groundLvl = 0;
    private float lvlWidth = 0;
    protected ChainShape rootShape;
    //    private Vector2 prevVert, nextVert;
    private LinkedList<Vector2> chainVerts;

    //    public Ground() {
    //        this(new En, 0);
    //    }

    public Terrain(float width, float groundLvl, EntityDef def) {
        super(def, BodyType.StaticBody);

        this.groundLvl = groundLvl;
        lvlWidth = width;

        chainVerts = new LinkedList<>();
        b2dCategory = PhysCat.NO_PASS.id;
        b2dMask = PhysCat.CHARACTER.id;
    }

//    public Ground(float width, float groundLvl) {
//        super("gl_trn_ground", BodyType.StaticBody);
//        this.groundLvl = groundLvl;
//        lvlWidth = width;
//
//        chainVerts = new LinkedList<>();
//        b2dCategory = PhysCat.NO_PASS.id;
//        b2dMask = PhysCat.CHARACTER.id;
//    }

    public void setGroundLvl(float height) {
        groundLvl = height;
    }

    public void setGroundWidth(float width) {
        lvlWidth = width;
    }

    //    public void addVertex(Vector2 vertex) {
    //        chainVerts.add(vertex);
    //    }

    public void addVertices(Vector2[] vertices) {
        ChainShape cs = new ChainShape();

        if (vertices != null && vertices.length > 0) {
            cs.createChain(vertices);

            FixtureDef fixDef = new FixtureDef();
            fixDef.shape = cs;
            fixDef.filter.categoryBits = b2dCategory;
            fixDef.filter.maskBits = b2dMask;
            fixDef.restitution = 0f;
            fixDef.friction = 0f;

            boundFixture = body.createFixture(fixDef);
            boundFixture.setUserData(new VJXData("bound", this));
        }
    }

    @Override
    public void collideWith(NewEntity other) {
    }

    @Override
    public void trigger(Fixture triggerFixture) {

    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    protected void createFixtures() {
        body.destroyFixture(boundFixture);
        rootShape = new ChainShape();

        if (chainVerts.size() == 0 && lvlWidth != 0) {
            chainVerts.add(new Vector2(0, groundLvl + 2000).scl(.01f));
            chainVerts.add(new Vector2(0, groundLvl).scl(.01f));
            chainVerts.add(new Vector2(lvlWidth, groundLvl).scl(.01f));
            chainVerts.add(new Vector2(lvlWidth, groundLvl + 2000).scl(.01f));
        }

        if (chainVerts.size() > 0) {
            rootShape.createChain(
                            chainVerts.toArray(new Vector2[chainVerts.size()]));

            FixtureDef fixDef = new FixtureDef();
            fixDef.shape = rootShape;
            fixDef.filter.categoryBits = b2dCategory;
            fixDef.filter.maskBits = b2dMask;
            fixDef.restitution = 0f;
            fixDef.friction = 0f;

            boundFixture = body.createFixture(fixDef);
            boundFixture.setUserData(new VJXData("bound", this));

            rootShape = (ChainShape) boundFixture.getShape();
        }
        boundShape = rootShape;
    }

    public int getChainSize() {
        return chainVerts.size();
    }

    @Override
    public void spawnActor(World world) {

    }

    @Override
    protected void updateFixtures() {
        // TODO Auto-generated method stub

    }
}