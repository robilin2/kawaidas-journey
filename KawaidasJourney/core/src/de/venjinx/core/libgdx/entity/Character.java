package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;

public abstract class Character extends AnimatedEntity {

    protected int health = 1;
    protected int maxHP = 1;
    protected float moveSpeed = 1f;

    protected boolean attacking = false;

    public Character(EntityDef def, BodyType type) {
        super(def, type);
    }

    //    public Character(String who, BodyType type) {
    //        super(who, type);
    //    }

    public void walk() {
        Vector2 bodyVel = body.getLinearVelocity();
        bodyVel.x = moveSpeed;
        body.setLinearVelocity(bodyVel);

        if (isOnGround() && status != Status.WALK) {
            setStatus(Status.WALK);
        }
    }

    public void run() {
        Vector2 bodyVel = body.getLinearVelocity();
        bodyVel.x = moveSpeed;
        body.setLinearVelocity(bodyVel);

        if (isOnGround() && status != Status.RUN) {
            setStatus(Status.RUN);
        }
    }

    public void stop() {
        if (isOnGround()) setStatus(Status.IDLE);

        Vector2 bodyVel = body.getLinearVelocity();

        body.setLinearVelocity(0, bodyVel.y);
    }

    @Override
    public void setDirection(VJXDirection direction) {
        super.setDirection(direction);
        switch (direction) {
        case Left:
            moveSpeed = -Math.abs(moveSpeed);
            break;
        case Right:
            moveSpeed = Math.abs(moveSpeed);
            break;
        case Down:
            break;
        case Up:
            break;

        default:
            break;
        }
    }

    public void incrHealth() {
        health++;
    }

    public void decrHealth() {
        health--;
    }

    public void setHealth(int newHealth) {
        health = newHealth;
    }

    public int getHealth() {
        return health;
    }

    public void setMaxHP(int newMaxHP) {
        maxHP = newMaxHP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public float getSpeed() {
        return moveSpeed;
    }

    public void setSpeed(float speed) {
        moveSpeed = speed;
    }
}