package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.kawaida.kjgame.animation.AnimationSet;
import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.actors.Kawaida;
import de.kawaida.kjgame.entities.actors.Neutral;
import de.kawaida.kjgame.entities.items.Item;
import de.kawaida.kjgame.entities.items.Weapon;
import de.venjinx.core.libgdx.VJXAssetManager;
import de.venjinx.core.libgdx.VJXGame;

public class EntityFactory {

    //    private HashMap<Long, NewEntity> entities = new HashMap<>();
    private VJXAssetManager am;
    private VJXGame game;

    public EntityFactory(VJXGame game) {
        this.game = game;
        am = game.assetMngr;
    }

    public NewEntity createEntity(EntityDef def) {
        if (def == null) return null;

        NewEntity e;

        TextureAtlas ta;
        AnimationSet anims;

        switch (def.getCategory()) {
        case STAGE:
            e = new Terrain(game.getScreen().getLevel().getLvlWidth(), 250,
                            def);
            break;
        case AVATAR:
            Kawaida a = new Kawaida(game, def);

            EntityDef eDef = EntityDef.fromUniqueName("gl_itm_banana");
            Item i = (Item) game.factory.createEntity(eDef);
            i.set(1);
            i.setMax(4);
            game.getPlayer().setBanana(i);

            eDef = EntityDef.fromUniqueName("gl_itm_totem");
            i = (Item) game.factory.createEntity(eDef);
            game.getPlayer().setTotem(i);

            eDef = EntityDef.fromUniqueName("gl_itm_sun");
            i = (Item) game.factory.createEntity(eDef);
            i.setMax(-1);
            game.getPlayer().setSun(i);

            a.setHealth(1);
            a.setMaxHP(4);

            e = a;
            break;
        case ENEMY:
            e = new Enemy(def);
            break;
        case ITEM:
            e = new Item(def);
            break;
        case NEUTRAL:
            e = new Neutral(def);

            break;
        case WEAPON:
            e = new Weapon(def);
            break;
        case TERRAIN:
            if (def.getSubcategory().contains("tiled"))
                e = new TiledEntity(def, BodyType.KinematicBody);
            else e = new Obstacle(def);

            //            entities.put(e.getID(), e);
            return e;
        //            break;
        default:
            return null;
        }

        //        entities.put(e.getID(), e);

        if (def.getSubcategory().contains("utility")) return e;

        ta = am.getSpriteSheet("anims_" + def.getUniqueName());
        anims = new AnimationSet((AnimatedEntity) e);
        anims.load(ta);

        ((AnimatedEntity) e).setAnimations(anims);

        anims.getAnimation(def.getUniqueName() + "_idle").setLoop(true);

        String[] s = def.getActivities().split(",");
        if (s.length > 0 && !s[0].isEmpty()) {
            for (String str : s) {
                anims.getAnimation(def.getUniqueName() + "_" + str).setLoop(true);
            }
        }

        return e;
    }

    public void destroyEntity(NewEntity e) {
        //        if (!entities.containsKey(id)) return;

        //        NewEntity e = entities.remove(id);

        e.getActor().remove();
        e.despawn();

        game.getScreen().getLevel().getB2DWorld().destroyBody(e.getBody());
    }

    //    public void destroyEntity(long id) {
    //        //        if (!entities.containsKey(id)) return;
    //
    //        //        NewEntity e = entities.remove(id);
    //
    //        e.getActor().remove();
    //        e.despawn();
    //
    //        game.getScreen().getLevel().getB2DWorld().destroyBody(e.getBody());
    //    }
}