package de.venjinx.core.libgdx.entity;

import java.util.HashSet;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.kawaida.kjgame.entities.level.GameWorld;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.kawaida.kjgame.interfaces.Visitor;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXUtility.Status;
import de.venjinx.core.libgdx.VJXUtility.StatusChangedEvent;
import de.venjinx.core.libgdx.VJXUtility.VJXDirection;
import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.assets.AvatarIcon;

public abstract class Entity extends Actor implements Acceptor {

    protected final static HashSet<AssetDef<? extends Object>> resources = new HashSet<>();

    // unique identifier managed by VJXGame.class
    private long id = -1;

    // indicates that the entity changed its activity
    private boolean statusChanged = false;

    // entity game status: spawned > alive -> always in in world
    protected boolean spawned = false;  //  not in world->spawn->in world->despawn
    protected boolean alive = false;    // spawn->alive->die->not alive->respawn

    // physics category and collision mask
    protected short b2dCategory = 0;
    protected short b2dMask = 0;

    // entity world status
    protected Status status = Status.IDLE;

    // the direction entity is facing and moving
    protected VJXDirection direction = VJXDirection.Right;

    // if 0 is not touching any ground
    protected int onGround = 0;

    // the entities icon and current actively drawn image
    protected AvatarIcon icon;
    protected AtlasRegion frame;

    // the entities bounding box definition and shape
    protected FixtureDef boundDef;
    protected Fixture boundFixture;
    protected Shape boundShape;

    // the entities physical body
    protected BodyType type;
    protected Body body;

    protected EntityDef definition;

    // transform for drawing the current image at the correct location
    // relative to the entities center-based physical position
    // body position -> draw position (bottom-left -> up-right)
    protected Affine2 drawTransform = new Affine2();

    public Entity(EntityDef def, BodyType type) {
        this.type = type;
        definition = def;
        id = VJXGame.newID();
        setName(def.getUniqueName());

        boundDef = new FixtureDef();
        boundDef.restitution = 0f;
        boundDef.friction = 0f;
    }

    //    public Entity(String who, BodyType type) {
    //        this.type = type;
    //        id = VJXGame.newID();
    //        setName(who);
    //
    //        boundDef = new FixtureDef();
    //        boundDef.restitution = 0f;
    //        boundDef.friction = 0f;
    //    }

    public abstract void collideWith(Entity other);

    public abstract void trigger(Fixture triggerFixture);

    public abstract void userAct(float deltaT);

    public abstract void userDraw(Batch batch, float parentAlpha);

    protected abstract void createFixtures();

    public abstract void spawnActor(World world);

    @Override
    public void act(float deltaT) {
        updateBody = body != null;

        super.act(deltaT);

        //        Vector2 newPos = new Vector2();
        //
        //        if (body != null) newPos.set(body.getPosition()).scl(100f);
        //
        //        newPos.sub(getWidth() / 2, getHeight() / 2);
        //
        //        super.setPosition(newPos.x, newPos.y);

        userAct(deltaT);

        updateDrawTransform();
    }

    private boolean updateBody = true;

    @Override
    public void positionChanged() {
        updateDrawTransform();

        if (updateBody) {
            updateBody = false;

            if (getName().contains("plat")) System.out.println("set pos");
            body.setTransform(drawTransform.getTranslation(new Vector2())
                            //                            .add(getWidth() / 2f, getHeight() / 2f)
                            .scl(1 / 100f), 0);
        }
    }

    //    @Override
    //    public void setPosition(float x, float y) {
    //        super.setPosition(x, y);
    //        updateDrawTransform();
    //
    //
    //        if (body != null) {
    //            if (getName().contains("plat")) System.out.println("set pos");
    //            body.setTransform(drawTransform.getTranslation(new Vector2())
    //                            //                            .add(getWidth() / 2f, getHeight() / 2f)
    //                            .scl(1 / 100f), 0);
    //
    //        }
    //    };

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (frame != null) batch.draw(frame, frame.getRegionWidth(),
                        frame.getRegionHeight(), drawTransform);
        userDraw(batch, parentAlpha);
    }

    @Override
    public void accVisitor(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accCollision(Visitor visitor) {
        visitor.hit(this);
    }

    @Override
    public void accTrigger(Visitor visitor) {
        visitor.trigger(this);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        setOrigin(width / 2f, height / 2f);
    }

    public Body spawn(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;

        float cx = getX() + getOriginX();
        float cy = getY() + getOriginY();

        body = world.createBody(bodyDef);
        body.setSleepingAllowed(false);
        //        body.setUserData(new VJXData(getName(), this));
        body.setTransform(cx / 100f, cy / 100f, 0);

        //        setStatus(VJXStatus.SPAWN, true);
        setStatus(Status.IDLE, true);

        // create entity hit boxes
        createBound();

        // create custom fixtures from classes extending this class
        createFixtures();

        spawned = true;
        alive = true;

        return body;
    }

    public void despawn() {
        spawned = false;
        alive = false;

        if (body != null) {
            body.getWorld().destroyBody(body);
            body = null;
        }
    }

    public long getID() {
        return id;
    }

    public void setStatus(Status status) {
        setStatus(status, false);
    }

    StatusChangedEvent statusEvent = new StatusChangedEvent();

    public void setStatus(Status status, boolean force) {
        if (this.status == status) return;
        if (this.status.blocking && !force) return;

        statusEvent.reset();
        statusEvent.oldStatus = this.status;
        statusEvent.newStatus = status;
        statusEvent.setTarget(this);

        notify(statusEvent, false);

        if (status == Status.DIE) {
            alive = false;
            body.setLinearVelocity(0, 0);

            //            for (Fixture f : body.getFixtureList())
            //                if (this instanceof Enemy) {
            //                    b2dCategory = PhysCat.CHARACTER.id;
            //                    b2dMask = PhysCat.NO_PASS.id;
            //
            //                    Filter filter = new Filter();
            //                    filter.categoryBits = b2dCategory;
            //                    filter.maskBits = b2dMask;
            //                    f.setFilterData(filter);
            //                }
        }

        this.status = status;
        statusChanged = true;
    }

    public boolean statusChanged() {
        return statusChanged;
    }

    public void setUnchanged() {
        statusChanged = false;
    }

    public Status getStatus() {
        return status;
    }

    public void setAvatarIcon(AvatarIcon icon) {
        this.icon = icon;
    }

    public AvatarIcon getAvatarIcon() {
        return icon;
    }

    public void setFrame(AtlasRegion f) {
        frame = f;
        if (frame != null) {
            setSize(frame.originalWidth, frame.originalHeight);
        }
    }

    public void setDirection(VJXDirection direction) {
        this.direction = direction;
    }

    public void resetOnGround() {
        onGround = 0;
    }

    public void incrOnGround() {
        onGround++;
    }

    public void decrOnGround() {
        onGround = onGround > 0 ? onGround - 1 : onGround;
    }

    public boolean isOnGround() {
        return onGround > 0;
    }

    public void setCategory(short category) {
        b2dCategory = category;

        updateFilter();
    }

    public short getB2DCategory() {
        return b2dCategory;
    }

    public void setCollisionMask(short mask) {
        b2dMask = mask;

        updateFilter();
    }

    public void setSensor(boolean sensor) {
        for (Fixture f : body.getFixtureList())
            f.setSensor(sensor);
    }

    public short getCollisionMask() {
        return b2dMask;
    }

    public void setPhysics(short category, short mask) {
        setCategory(category);
        setCollisionMask(mask);
    }

    private void updateFilter() {
        for (Fixture f : body.getFixtureList())
            if (!f.isSensor()) {
                Filter filter = new Filter();
                filter.categoryBits = b2dCategory;
                filter.maskBits = b2dMask;
                f.setFilterData(filter);
            }
    }

    public Body getBody() {
        return body;
    }

    public float getDrawX() {
        if (frame == null) return 0f;
        return getX() + frame.offsetX;
    }

    public float getDrawY() {
        if (frame == null) return 0f;
        return getY() + frame.offsetY;
    }

    public Vector2 getDrawPosition() {
        if (frame == null) return new Vector2(getX(), getY());

        return new Vector2(getX() + frame.offsetX,
                           getY() + frame.offsetY);
    }

    public Vector2 getCenter() {
        return new Vector2(getX() + getOriginX(), getY() + getOriginY());
    }

    public GameWorld getWorld() {
        return (GameWorld) super.getStage();
    }

    public VJXDirection getFaceDirection() {
        return direction;
    }

    public boolean isSpawned() {
        return spawned;
    }

    public boolean isAlive() {
        return alive;
    }

    protected void resizeBound() {
        resizeBound(frame);
    }

    protected void resizeBound(AtlasRegion frame) {
        if (frame != null) resizeBound(frame.getRegionWidth(),
                        frame.getRegionHeight(), frame.offsetX, frame.offsetY);
    }

    protected void resizeBound(float w, float h, float offX, float offY) {
        if (boundShape != null) {
            Vector2 center = new Vector2(0f, 0f);

            center.set(getX(), getY());
            center.add(offX, offY).add(getWidth() / 2f, getHeight() / 2f);
            center.sub(getCenter());
            center.scl(1f / 100f);

            if (boundShape instanceof PolygonShape) {
                ((PolygonShape) boundShape).setAsBox(w / 200f, h / 200f, center,
                                0);
                return;
            }

            if (boundShape instanceof ChainShape) {
                //                ((ChainShape) boundShape).createChain(new Vector2[] {
                //                                new Vector2(0, offY / 100f),
                //                                new Vector2(w / 100f, offY / 100f) });
                return;
            }
        }
    }

    protected void updateDrawTransform() {
        if (frame == null) return;
        // update the draw transform, draw pos is bottom left corner of object
        drawTransform.idt();
        drawTransform.translate(getX(), getY());

        // mirror the image if object is facing left
        if (direction == VJXDirection.Left) {
            drawTransform.translate(frame.getRegionWidth(), 0f);
            drawTransform.scale(-1, 1);
        }
    }

    private void createBound() {
        float halfW, halfH;
        Vector2 center = new Vector2();

        halfW = getWidth() / 2f;
        halfH = getHeight() / 2f;

        center.set(getX(), getY());
        center.add(halfW, halfH).sub(center);
        center.scl(1f / 100f);

        boundShape = new PolygonShape();
        ((PolygonShape) boundShape).setAsBox(halfW / 100f, halfH / 100f, center, 0);

        boundDef.filter.categoryBits = b2dCategory;
        boundDef.filter.maskBits = b2dMask;
        boundDef.shape = boundShape;

        boundFixture = body.createFixture(boundDef);
        //        boundFixture.setUserData(new VJXData("bound", this));

        boundShape = boundFixture.getShape();

        //        BodyEditorLoader loader = new BodyEditorLoader(
        //                        Gdx.files.internal(""));
        //        loader.attachFixture(body, "bound", boundDef, 1);
    }

    public String getInfo() {
        String s = toString();

        s += "\n  BodyType   : " + type;
        s += "\n  Status     : " + status;
        s += "\n  Face       : " + direction;
        s += "\n  Category   : " + b2dCategory;
        s += "\n  Size       : [" + getWidth() + ":" + getHeight() + "]";
        s += "\n  Position   : [" + getX() + ":" + getY() + "]";
        s += "\n  DrawPos    : " + getDrawPosition();
        s += "\n  Center     : " + getCenter();
        s += "\n  Origin     : [" + getOriginX() + ":" + getOriginY() + "]";
        s += "\n  UserData   : " + getUserObject();
        s += "\n  Body       : " + body;
        if (body != null) {
            s += "\n  BodyCenter : " + body.getLocalCenter();
            s += "\n  BodyPos    : " + body.getPosition();
            s += "\n  BodyTrans  : " + body.getTransform().getPosition();
            s += "\n  BodyWCenter: " + body.getWorldCenter();
        }

        s += "\n";

        return s;
    }
}