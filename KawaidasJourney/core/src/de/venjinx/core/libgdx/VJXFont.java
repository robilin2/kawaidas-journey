package de.venjinx.core.libgdx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Disposable;

public class VJXFont implements Disposable {

    BitmapFont solid;
    BitmapFont outline;
    BitmapFont shadow;

    LabelStyle styleSolid;
    LabelStyle styleOutline;
    LabelStyle styleShadow;

    public VJXFont(BitmapFont solid, BitmapFont outline, BitmapFont shadow) {
        this.solid = solid;
        this.outline = outline;
        this.shadow = shadow;

        styleSolid = new LabelStyle(solid, Color.WHITE);

        styleOutline = new LabelStyle(outline, Color.BLACK);

        styleShadow = new LabelStyle(shadow, Color.BLACK);
    }

    public void setSolid(BitmapFont solid) {
        if (solid == null || this.solid.equals(solid)) return;

        styleSolid = new LabelStyle(solid, Color.WHITE);

        this.solid.dispose();
        this.solid = solid;
    }

    public void setOutline(BitmapFont outline) {
        if (outline == null || this.outline.equals(outline)) return;

        styleOutline = new LabelStyle(outline, Color.WHITE);

        this.outline.dispose();
        this.outline = outline;
    }

    public void setShadow(BitmapFont shadow) {
        if (shadow == null || this.shadow.equals(shadow)) return;

        styleShadow = new LabelStyle(shadow, Color.WHITE);

        this.shadow.dispose();
        this.shadow = shadow;
    }

    public BitmapFont getFontSolid() {
        return solid;
    }

    public BitmapFont getFontOutline() {
        return outline;
    }

    public BitmapFont getFontShadow() {
        return shadow;
    }

    public LabelStyle getStyleSolid() {
        return styleSolid;
    }

    public LabelStyle getStyleOutline() {
        return styleOutline;
    }

    public LabelStyle getStyleShadow() {
        return styleShadow;
    }

    @Override
    public void dispose() {
        styleSolid = null;
        styleOutline = null;
        styleShadow = null;

        solid.dispose();
        outline.dispose();
        shadow.dispose();
    }
}