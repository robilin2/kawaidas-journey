package de.venjinx.core.libgdx;

import java.util.ArrayDeque;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.kawaida.kjgame.controllers.CollisionControl;
import de.kawaida.kjgame.screens.GameScreen;
import de.kawaida.ui.skins.MenuSkin;
import de.venjinx.core.libgdx.audio.AudioController;
import de.venjinx.core.libgdx.entity.EntityFactory;
import de.venjinx.core.libgdx.entity.EntityListener;

public class VJXGame extends Game {

    private static long currentID = 0;
    private static ArrayDeque<Long> freeIDs = new ArrayDeque<Long>();

    public Vector2 res = new Vector2();
    public Vector2 targetRes = new Vector2();

    private float timePassed = 0;
    private long timeUpdate = 0;
    private long timeRender = 0;

    private float step = 1f / 60f;

    private OrthographicCamera cam;
    private OrthographicCamera interfaceCam;

    private FitViewport view;
    private FitViewport interfaceView;
    private FreeTypeFontGenerator generator;

    public CollisionControl cCtrl;
    public EntityListener entityListener;

    public SpriteBatch batch;
    public VJXFont font;
    public VJXAssetManager assetMngr;
    public EntityFactory factory;
    public AudioController sfx;

    public MenuSkin menuSkin;

    private GameScreen gameScreen;

    private boolean paused = false;

    private String[] args;

    public Preferences settings;

    public VJXGame() {
    }

    public VJXGame(String[] args) {
        this.args = args;

        //        this.args = new String[1];
        //        this.args[0] = "D:/Development/Projekte/Kawaida/Dev/kawaidas-journey/"
        //                        + "KawaidasJourney/android/assets/development/level/"
        //                        + "dev_lvl_00_test_level.tmx";
    }

    @Override
    public void create () {
        Gdx.input.setInputProcessor(new InputMultiplexer());

        res.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //        targetRes.set(960, 540);
        targetRes.set(1280, 720);

        cam = new OrthographicCamera();

        batch = new SpriteBatch();
        batch.setProjectionMatrix(cam.combined);

        interfaceCam = new OrthographicCamera();

        view = new FitViewport(targetRes.x, targetRes.y, cam);
        //        view.setScreenSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        interfaceView = new FitViewport(targetRes.x, targetRes.y, interfaceCam);
        //        interfaceView.setScreenSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        assetMngr = new VJXAssetManager();
        cCtrl = new CollisionControl();
        sfx = new AudioController();

        factory = new EntityFactory(this);
        entityListener = new EntityListener(this);

        loadFont();

        assetMngr.finishLoading();
        menuSkin = new MenuSkin(font, assetMngr);

        gameScreen = new GameScreen(this);
        setScreen(gameScreen);

        settings = Gdx.app.getPreferences("global_settings");
        settings.putBoolean("soundOn", true);
        settings.putBoolean("musicOn", true);

        if (args == null || args.length == 0)
            gameScreen.loadMenu("ui_btn_start");
        else gameScreen.loadLevel(args[0], true);

        //        Gdx.input.setCatchBackKey(true);
    }

    //    public long gameRender = 0;
    //    private float deltaScaled = 0;

    @Override
    public void render () {
        //        System.out.println("------------------begin main loop------------------");
        Gdx.gl20.glClearColor(0, 0, 0, 1);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!paused) {
            //            gameRender = System.currentTimeMillis();

            //            System.out.println("----------------------game update-----------------------");

            timePassed += Gdx.graphics.getDeltaTime();

            timeUpdate = System.currentTimeMillis();
            if (timePassed >= step) {
                //                    b2dWorld.step(step, 6, 2);
                getScreen().update(Gdx.graphics.getDeltaTime());
                timePassed -= step;
            }
            timeUpdate = System.currentTimeMillis() - timeUpdate;

            //                dbgStage.act(nextStep);

            //            System.out.println("--------------------game update end---------------------\n");

            //            System.out.println("----------------------game render-----------------------");

            //            System.out.println("    -----------------screen render------------------    ");
            //            dbgStage.draw();
            //            System.out.println("    ---------------screen render end----------------    ");

            //            System.out.println("--------------------game render end---------------------\n\n");

            //        if (Gdx.input.isKeyPressed(Keys.BACK)) {
            //            dispose();
            //            Gdx.app.exit();
            //        }
            //            System.out.println(player.getListeners().size);
        }
        timeRender = System.currentTimeMillis();
        getScreen().render(Gdx.graphics.getDeltaTime());
        //        System.out.println("-------------------end main loop-------------------\n");
    }

    @Override
    public GameScreen getScreen() {
        return (GameScreen) super.getScreen();
    }

    @Override
    public void dispose() {
        super.dispose();
        font.dispose();
        generator.dispose();
        batch.dispose();
        if (gameScreen != null) gameScreen.dispose();
        sfx.dispose();
        assetMngr.dispose();
    }

    @Override
    public void pause() {
        paused = true;
        screen.pause();
    }

    @Override
    public void resume() {
        paused = false;
        screen.resume();
    }

    @Override
    public void resize(int width, int height) {
        res.set(width, height);
        super.resize(width, height);
        view.update(width, height, true);
        interfaceView.update(width, height, true);
    }

    private void loadFont() {
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = (int) Math.ceil(25);
        parameter.minFilter = TextureFilter.Nearest;
        parameter.magFilter = TextureFilter.MipMapLinearNearest;

        BitmapFont s, out, sh;
        String fileName = "global/stage/font/londrina/LondrinaSolid-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        s = generator.generateFont(parameter);

        parameter.size = (int) Math.ceil(25);
        fileName = "global/stage/font/londrina/LondrinaOutline-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        out = generator.generateFont(parameter);

        parameter.size = (int) Math.ceil(25);
        fileName = "global/stage/font/londrina/LondrinaShadow-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        sh = generator.generateFont(parameter);

        font = new VJXFont(s, out, sh);
    }

    public Player getPlayer() {
        return gameScreen.getPlayer();
    }

    public OrthographicCamera getCam() {
        return cam;
    }

    public OrthographicCamera getInterfaceCam() {
        return interfaceCam;
    }

    public FitViewport getView() {
        return view;
    }

    public FitViewport getInterfaceView() {
        return interfaceView;
    }

    public float getTimePassed() {
        return timePassed;
    }

    public long getTimeUpdate() {
        return timeUpdate;
    }

    public float getTimeRender() {
        return timeRender;
    }

    public static long newID() {
        long id = -1;
        if (!freeIDs.isEmpty()) id = freeIDs.removeFirst();
        else id = currentID++;

        return id;
    }

    public static long freeID(long id) {
        freeIDs.add(id);

        return id;
    }
}