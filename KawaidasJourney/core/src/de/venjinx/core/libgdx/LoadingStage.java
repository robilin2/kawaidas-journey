package de.venjinx.core.libgdx;

import java.util.ArrayDeque;
import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.kawaida.kjgame.stages.Menu;
import de.kawaida.ui.widgets.KJLabel;

public class LoadingStage extends VJXStage {

    private VJXStage currentStage;

    private ArrayDeque<VJXStage> stagesToLoad;
    private ArrayDeque<VJXStage> stagesToUnload;
    private ArrayDeque<VJXStage> loadedStages;

    private Table ui;
    private String loadStr = "Please wait...loading - ";
    private KJLabel progressLabel;

    private boolean processing = false;
    private boolean loading = false;
    private boolean unloading = false;

    public LoadingStage(VJXGame game) {
        super(game);

        resources.put("lvl_loading", new AssetDescriptor<>(
                        "kizim_kazi/stage/kzk_stg_lvl0_loading.jpg", Texture.class));
        resources.put("splash", new AssetDescriptor<>("menu/menu_splash.jpg",
                        Texture.class));

        stagesToLoad = new ArrayDeque<>();
        stagesToUnload = new ArrayDeque<>();
        loadedStages = new ArrayDeque<>();

        prepare();
    }

    public float getProgress() {
        int progress = (int) (game.assetMngr.getProgress() * 10000);

        return progress / 100f;
    }

    @Override
    protected void init() {
        for (String s : resources.keySet()) {
            if (!game.assetMngr.isLoaded(s))
                game.assetMngr.loadResource(s, resources.get(s));
        }
        game.assetMngr.finishLoading();

        clear();

        ui = new Table(game.menuSkin);
        ui.setBackground(game.assetMngr.createSpriteDrawable("splash"));
        ui.setFillParent(true);

        progressLabel = new KJLabel(loadStr, game.menuSkin);
        ui.add(progressLabel);

        addActor(ui);
    }

    public void addToUnloadingQueue(VJXStage stage) {
        stage.setActive(false);
        stage.setRunBackground(true);

        stagesToUnload.add(stage);
    }

    public void addToLoadingQueue(VJXStage stage) {
        stagesToLoad.add(stage);
    }

    //    public void loadResources(HashSet<AssetDef<? extends Object>> res) {
    //        for (AssetDef<? extends Object> ad : res) {
    //            if (!game.assetMngr.isLoaded(ad.fileName))
    //                game.assetMngr.loadResource(ad);
    //        }
    //        process(true);
    //    }

    public void unloadResources(HashMap<String, AssetDescriptor<? extends Object>> res) {
        for (String s : res.keySet()) {
            if (!game.assetMngr.isLoaded(s)) game.assetMngr.unloadResource(s);
        }
        process(true);
    }

    public void process(boolean background) {
        processing = processNext();

        if (!processing) return;

        setActive(true);
        setRunBackground(background);


        if (!background) {
            if (currentStage instanceof Menu) ui.setBackground(
                            game.assetMngr.createSpriteDrawable("loading"));
            else ui.setBackground(
                            game.assetMngr.createSpriteDrawable("lvl_loading"));
        }
    }

    private boolean processNext() {
        if (loading || unloading) return processing = true;

        currentStage = null;

        loading = false;
        unloading = false;

        if (!stagesToUnload.isEmpty()) {
            currentStage = stagesToUnload.pollFirst();

            for (String s : currentStage.getResources().keySet()) {
                game.assetMngr.unloadResource(s);
            }

            unloading = true;

            return processing = true;
        }

        if (!stagesToLoad.isEmpty()) {
            currentStage = stagesToLoad.pollFirst();

            for (String s : currentStage.getResources().keySet()) {
                game.assetMngr.loadResource(s,
                                currentStage.getResources().get(s));
            }

            loading = true;

            return processing = true;
        }

        if (game.assetMngr.getQueuedAssets() > 0) return processing = true;

        return processing = false;
    }

    @Override
    protected void preAct(float delta) {

        if (processing) {

            game.assetMngr.update();
            progressLabel.setText(loadStr + getProgress() + "%");

            if (game.assetMngr.getProgress() == 1f) {
                if (loading) {
                    currentStage.prepare();
                    loadedStages.add(currentStage);
                }

                loading = false;
                unloading = false;

                processing = processNext();

                if (!processing) {
                    game.sfx.fetchSounds(game.assetMngr);
                    progressLabel.setText("Finished loading. Click to start.");
                }

                //            System.out.println("Finished: " + currentStage);
                //            System.out.println("----------------------------------");
                //            System.out.println();
            }
        }
    }

    @Override
    protected void postAct(float delta) {

    }

    @Override
    protected void preDraw() {

    }

    @Override
    protected void postDraw() {

    }

    public boolean isProcessing() {
        return processing;
    }

    public ArrayDeque<VJXStage> getLoadedStages() {
        return loadedStages;
    }

    public boolean hasNewData() {
        return !loadedStages.isEmpty() && !processing;
    }

    @Override
    public void dispose() {
        clear();

        for (String s : resources.keySet()) {
            game.assetMngr.unloadResource(s);
        }
        game.assetMngr.finishLoading();

        currentStage = null;

        stagesToLoad.clear();
        stagesToLoad = null;

        stagesToUnload.clear();
        stagesToUnload = null;

        loadedStages.clear();
        loadedStages = null;

        ui = null;
        loadStr = null;
        progressLabel = null;
    }
}