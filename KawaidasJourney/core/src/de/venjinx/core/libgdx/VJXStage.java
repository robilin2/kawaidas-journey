package de.venjinx.core.libgdx;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class VJXStage extends Stage {

    private String name = "VJXStage";

    protected VJXGame game;

    protected HashMap<String, AssetDescriptor<? extends Object>> resources;

    private boolean active = false;
    private boolean runBackground = true;
    private boolean prepared = false;

    public VJXStage(VJXGame game) {
        super(game.getView(), game.batch);
        this.game = game;

        resources = new HashMap<>();
    }

    @Override
    public void act(float delta) {
        if (!active) return;

        preAct(delta);
        super.act(delta);
        postAct(delta);
    }

    @Override
    public void draw() {
        if (runBackground) return;

        preDraw();
        super.draw();
        postDraw();
    }

    protected void prepare() {
        init();
        prepared = true;
    }

    protected abstract void init();

    protected abstract void preAct(float delta);

    protected abstract void postAct(float delta);

    protected abstract void preDraw();

    protected abstract void postDraw();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public VJXGame getGame() {
        return game;
    }

    public HashMap<String, AssetDescriptor<? extends Object>> getResources() {
        return resources;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setRunBackground(boolean background) {
        runBackground = background;
    }

    public boolean isActive() {
        return active;
    }

    public boolean runBackground() {
        return runBackground;
    }

    public boolean isPrepared() {
        return prepared;
    }

    @Override
    public void dispose() {
        prepared = false;

        super.dispose();
    }

    @Override
    public String toString() {
        String s = this.getClass().getSimpleName() + " - " + getName();
        s += " - prepared: " + prepared;

        return s;
    }
}