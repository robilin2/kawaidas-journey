package de.venjinx.core.libgdx;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.AbsoluteFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class VJXAssetManager extends AssetManager {

    public TmxLoader intMapLoader;
    public TmxLoader absMapLoader;
    private HashMap<String, AssetDescriptor<? extends Object>> assetDescr;

    public VJXAssetManager() {
        assetDescr = new HashMap<>();

        intMapLoader = new TmxLoader(new InternalFileHandleResolver());
        absMapLoader = new TmxLoader(new AbsoluteFileHandleResolver());
    }

    public synchronized void loadResource(String name,
                    AssetDescriptor<? extends Object> descr) {
        if (!assetDescr.containsKey(name)) {
            load(descr);
            assetDescr.put(name, descr);
        }
    }

    public synchronized void unloadResource(String name) {
        if (assetDescr.containsKey(name)) {
            unload(assetDescr.get(name).fileName);
            assetDescr.remove(name);
        }
    }

    public synchronized TiledMap loadMap(String path, String name, boolean internal) {
        TiledMap m;
        if (internal) m = intMapLoader.load(path);
        else {
            m = absMapLoader.load(path);
        }

        addAsset(name, TiledMap.class, m);
        return m;
    }

    @Override
    public synchronized boolean isLoaded(String name) {
        if (!assetDescr.containsKey(name)) return false;
        return super.isLoaded(assetDescr.get(name).fileName);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public synchronized boolean isLoaded(String name, Class type) {
        if (!assetDescr.containsKey(name)) return false;
        return super.isLoaded(assetDescr.get(name).fileName, type);
    }

    public synchronized Texture getTexture(String name) {
        return (Texture) get(assetDescr.get(name).fileName);
    }

    public synchronized Drawable drawableFromSheet(String sheet, String name) {
        TextureRegionDrawable d = new TextureRegionDrawable(getRegion(sheet, name));
        return d;
    }

    public synchronized Sprite createSprite(String name) {
        return new Sprite(getTexture(name));
    }

    public synchronized SpriteDrawable createSpriteDrawable(String name) {
        return new SpriteDrawable(createSprite(name));
    }

    public synchronized Sound getSound(String name) {
        return get(assetDescr.get(name).fileName, Sound.class);
    }

    public synchronized Music getMusic(String name) {
        return get(assetDescr.get(name).fileName, Music.class);
    }

    public synchronized AtlasRegion getRegion(String sheet, String name) {
        TextureAtlas ta = get(assetDescr.get(sheet).fileName, TextureAtlas.class);

        return ta.findRegion(name);
    }

    public synchronized TextureAtlas getSpriteSheet(String name) {
        return get(assetDescr.get(name).fileName, TextureAtlas.class);
    }

    public synchronized TiledMap getMap(String name) {
        return get(name, TiledMap.class);
    }

    public synchronized void disposeMap(String name) {
        if (isLoaded(name)) {
            unload(name);
        }
    }
}