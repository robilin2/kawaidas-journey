package de.venjinx.core.libgdx;

import de.venjinx.core.libgdx.entity.NewEntity;

public class VJXData {

    private String name;
    private NewEntity entity;

    public VJXData(String name, NewEntity entity) {
        this.name = name;
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public NewEntity getEntity() {
        return entity;
    }
}