package de.venjinx.core.libgdx;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import de.kawaida.kjgame.entities.actors.Kawaida;
import de.kawaida.kjgame.entities.items.Item;
import de.kawaida.kjgame.entities.items.Weapon;

public class Player {

    private String name;

    private Kawaida avatar;

    private Weapon weapon;

    private Item sun;
    private Item totem;
    private Item banana;

    public Player(String name, VJXGame game) {
        setName(name);
    }

    public HashMap<String, AssetDescriptor<? extends Object>> fetchRes(
                    HashMap<String, AssetDescriptor<? extends Object>> res) {

        // item animations and sounds
        String dir = "global/item/";
        String name = "anims_gl_itm_banana";
        res.put(name, new AssetDescriptor<>(dir + "consumable/banana/"
                        + name + ".atlas",
                        TextureAtlas.class));
        name = "sfx_gl_itm_banana_collected";
        res.put(name, new AssetDescriptor<>(dir + "consumable/banana/sfx/"
                        + name + ".mp3",
                        Sound.class));

        name = "anims_gl_itm_totem";
        res.put(name, new AssetDescriptor<>(dir + "consumable/totem/"
                        + name + ".atlas",
                        TextureAtlas.class));
        name = "sfx_gl_itm_totem_collected";
        res.put(name, new AssetDescriptor<>(dir + "consumable/totem/sfx/"
                        + name + ".mp3",
                        Sound.class));

        name = "anims_gl_itm_cocos";
        res.put(name, new AssetDescriptor<>(dir + "valuable/cocos/"
                        + name + ".atlas",
                        TextureAtlas.class));
        name = "sfx_gl_itm_cocos_collected";
        res.put(name, new AssetDescriptor<>(dir + "valuable/cocos/sfx/"
                        + name + ".mp3",
                        Sound.class));

        name = "anims_gl_itm_sun";
        res.put(name, new AssetDescriptor<>(dir + "valuable/sun/"
                        + name + ".atlas",
                        TextureAtlas.class));
        name = "sfx_gl_itm_sun_collected";
        res.put(name, new AssetDescriptor<>(dir + "valuable/sun/sfx/"
                        + name + ".mp3",
                        Sound.class));

        name = "anims_gl_wpn_cocos";
        res.put(name, new AssetDescriptor<>(dir + "usable/weapon/cocos/"
                        + name + ".atlas",
                        TextureAtlas.class));
        name = "sfx_gl_wpn_cocos_hit";
        res.put(name, new AssetDescriptor<>(
                        dir + "usable/weapon/cocos/sfx/"
                        + name + ".mp3",
                        Sound.class));

        return res;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAvatar(Kawaida a) {
        avatar = a;
    }

    public Kawaida getAvatar() {
        return avatar;
    }

    public void setWeapon(Weapon w) {
        avatar.setWeapon(w);
        weapon = w;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setSun(Item sun) {
        this.sun = sun;
    }

    public Item getSun() {
        return sun;
    }

    public void setTotem(Item totem) {
        this.totem = totem;
    }

    public Item getTotem() {
        return totem;
    }

    public void setBanana(Item banana) {
        this.banana = banana;
    }

    public Item getBanana() {
        return banana;
    }
}