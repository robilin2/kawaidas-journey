package de.venjinx.core.libgdx;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;

import de.venjinx.core.libgdx.debug.DebugStage;

public abstract class VJXScreen implements Screen {

    private ArrayList<VJXStage> stages = new ArrayList<VJXStage>();

    protected VJXGame game;
    protected OrthographicCamera cam;

    protected boolean paused = false;

    private long frameNr = 0;

    private DebugStage dbgStage;

    protected LoadingStage loader;

    public VJXScreen(VJXGame game) {
        this.game = game;

        cam = game.getCam();

        dbgStage = new DebugStage(game);
        dbgStage.setRunBackground(false);

        ((InputMultiplexer) Gdx.input.getInputProcessor())
                        .addProcessor(dbgStage);

        loader = new LoadingStage(game);
        loader.addToLoadingQueue(dbgStage);
    }

    public abstract void restart();

    public void addStage(VJXStage stage) {
        if (!stages.contains(stage))
            stages.add(stage);
    }

    public boolean removeStage(VJXStage stage) {
        ((InputMultiplexer) Gdx.input.getInputProcessor())
                        .removeProcessor(stage);
        loader.addToUnloadingQueue(stage);
        return stages.remove(stage);
    }

    private void activateLoadedStages() {
        ArrayDeque<VJXStage> stages = loader.getLoadedStages();

        VJXStage s;
        while (!stages.isEmpty()) {
            s = stages.pollFirst();
            s.setActive(true);
            s.setRunBackground(false);
            ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(s);
        }

        loader.setRunBackground(true);

        game.pause();
    }

    public void update(float deltaT) {
        //            System.out.println("    -----------------screen update------------------    ");

        loader.act(deltaT);

        if (loader.hasNewData() && Gdx.input.isButtonPressed(0))
            activateLoadedStages();

        if (!paused) {
            frameNr++;

            for (VJXStage s : stages)
                s.act(deltaT);

            //            System.out.println("    ---------------screen update end----------------    \n");
        }
    }

    @Override
    public void render(float delta) {
        for (VJXStage s : stages)
            s.draw();

        dbgStage.draw();

        loader.draw();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void show() {
    }

    public boolean isPaused() {
        return paused;
    }

    @Override
    public void resize(int width, int height) {
    }

    public long getFrameNr() {
        return frameNr;
    }

    public void setDebug(boolean debug) {
        for (VJXStage s : stages)
            s.setDebugAll(debug);
    }

    public DebugStage getDebug() {
        return dbgStage;
    }

    @Override
    public void dispose() {
        VJXStage s;
        Iterator<VJXStage> iter = stages.iterator();
        while (iter.hasNext()) {
            s = iter.next();

            for (String str : s.getResources().keySet()) {
                    game.assetMngr.unloadResource(str);
            }
            game.assetMngr.finishLoading();
            s.dispose();
        }

        stages.clear();
        stages = null;

        loader.dispose();
        loader = null;

        game = null;
        cam = null;
    }
}