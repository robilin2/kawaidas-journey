package de.venjinx.core.libgdx;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Event;

public class VJXUtility {

    public static class StatusChangedEvent extends Event {

        public Status oldStatus;
        public Status newStatus;

        //        public StatusChangedEvent(Status newS, Status oldS) {
        //            newStatus = newS;
        //            oldStatus = oldS;
        //        }

        public String getName() {
            return newStatus.name;
        }
    }

    public enum Status {

        // passive blocking
        // actions
        SPAWN("spawn", 0, false, true), HIT("hit", 1, false, true),
        DIE("die", 2, false, true),

        // passive non-blocking
        // actions
        TRIGGERED("triggered", 3, false, false),

        // active blocking

        // active non-blocking
        // activity
        IDLE("idle", 4, true, false), ACTIVE("active", 5, true, false),
        WALK("walk", 6, true, false), RUN("run", 7, true, false),
        FLY("fly", 8, true, false), SWIM("swim", 9, true, false),
        CLIMB("climb", 10, true, false),

        // actions
        JUMP("jump", 11, false, false), ATTACK("attack", 12, false, false),
        THROW("throw", 13, false, false);

        public final String name;
        public final int id;
        public final boolean activity;
        public final boolean blocking;

        private Status(String name, int id, boolean activity, boolean blocking) {
            this.name = name;
            this.id = id;
            this.blocking = blocking;
            this.activity = activity;
        }

        public static final Status get(int id) {
            for (Status s : Status.values())
                if (s.id == id) return s;

            return null;
        }

        public static final Status get(String name) {
            for (Status s : Status.values())
                if (s.name.equals(name)) return s;

            return null;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " " + id + ": " + name + " - "
                            + blocking;
        }
    };

    public enum PhysCat {
        REMOVE("remove", Short.MIN_VALUE), NONE("none", (short) 0),
        PLAYER("player", (short) 1), CHARACTER("character", (short) 2),
        ITEM("item", (short) 4), TRIGGER("trigger",(short) 8),
        ENEMY("enemy", (short) 16), NO_PASS("no_pass", (short) 32),
        WEAPON("weapon", (short) 64), ALL("all", Short.MAX_VALUE);

        public final String name;
        public final short id;

        private PhysCat(String name, final short id) {
            this.name = name;
            this.id = id;
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };

    public enum Environment {
        DEVELOPMENT("development", "dev", 0), GLOBAL("global", "gl", 1),
        MENU("menu", "menu", 2), USER_INTERFACE("user_interface", "ui", 3),
        BONUS("bonus", "bon", 4), KIZIM_KAZI("kizim_kazi", "kzk", 5),
        JOZANI("jozani", "jz", 6), ZANZIBAR_TOWN("zanzibar", "znz", 7),
        PACIFIC("pacific", "pcf", 8), DAR_ES_SALAAM("dar_es_salaam", "dsm", 9),
        DAR_TRAFFIC("dar_traffic", "dtr", 10), MIKUMI_PARK("mikumi_park", "mkp", 11);

        public final String envName;
        public final String envShort;
        public final int id;

        private Environment(final String name, final String eShort, final int id) {
            envName = name;
            envShort = eShort;
            this.id = id;
        }

        public static final Environment get(int id) {
            for (Environment e : Environment.values())
                if (e.id == id) return e;

            return null;
        }

        public static final Environment get(String name) {
            for (Environment e : Environment.values())
                if (e.envName.equals(name) || e.envShort.equals(name)) return e;

            return null;
        }
    }

    public enum Category {
        STAGE("stage", "stg", 0), QUEST("quest", "qst", 1),
        AVATAR("avatar", "avt", 2), TERRAIN("terrain", "trn", 3),
        ITEM("item", "itm", 4), WEAPON("weapon", "wpn", 5),
        NEUTRAL("neutral", "ntrl", 6), ENEMY("enemy", "nmy", 7),
        PLANT("plant", "plt", 8), ROCK("rock", "rck", 9),
        TREE("tree", "tr", 10), HOUSE("house", "hse", 11);

        public final String catName;
        public final String catShort;
        public final int id;

        private Category(final String name, final String cShort, final int id) {
            catName = name;
            catShort = cShort;
            this.id = id;
        }

        public static final Category get(int id) {
            for (Category c : Category.values())
                if (c.id == id) return c;

            return null;
        }

        public static final Category get(String name) {
            for (Category c : Category.values())
                if (c.catName.equals(name) || c.catShort.equals(name)) return c;

            return null;
        }
    }

    public enum VJXDirection {
        Right("right", new Vector2(1f, 0f)), Up("up", new Vector2(0f, 1f)),
        Left("left", new Vector2(-1f, 0f)), Down("down", new Vector2(0f, -1f));

        public final String name;
        public final Vector2 direction;

        //        public final boolean flipX;
        //        public final boolean flipY;

        private VJXDirection(String name, Vector2 direction) {
            this.name = name;
            this.direction = direction;
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };
}