package de.venjinx.core.libgdx.debug;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.kawaida.ui.UiTable;
import de.venjinx.core.libgdx.entity.Entity;

public class EntityInfoView extends UiTable {

    private Label whoLabel;
    private Entity currentEntity;

    public EntityInfoView(Skin skin) {
        super(skin, "ui_entityView");

        whoLabel = new Label("hallo test", skin);
        add(whoLabel);
        setFillParent(false);
    }

    public void setEntity(Entity entity) {
        currentEntity = entity;

        whoLabel.setText(entity.getName());
    }

    @Override
    public void prepare() {

    }

}
