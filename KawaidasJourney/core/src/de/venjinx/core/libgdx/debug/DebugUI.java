package de.venjinx.core.libgdx.debug;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

public class DebugUI extends Window {

    private EntityInfoView entityView;

    public DebugUI(String title, Skin skin) {
        super(title, skin);

        entityView = new EntityInfoView(skin);

        add(entityView);
    }


}