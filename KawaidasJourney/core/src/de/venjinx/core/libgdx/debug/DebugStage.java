package de.venjinx.core.libgdx.debug;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.kawaida.kjgame.util.Util;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;

public class DebugStage extends VJXStage {

    private World world;

    private SpriteBatch batch;
    private Box2DDebugRenderer dbgRenderer;
    private ShapeRenderer shapeRenderer;

    private BitmapFont dbgFont;

    private boolean drawGrid = false;
    private boolean debugB2D = false;
    private boolean drawInfo = false;
    private Vector2 mouseScreen = new Vector2();
    private Vector2 mouseDisplay = new Vector2();

    private DebugUI ui;

    public DebugStage(VJXGame game) {
        super(game);

        setViewport(game.getInterfaceView());

        //        getViewport().update((int) game.res.x, (int) game.res.y,
        //                        true);

        batch = game.batch;
        dbgFont = new BitmapFont();

        dbgRenderer = new Box2DDebugRenderer();

        shapeRenderer = new ShapeRenderer();
    }

    @Override
    protected void init() {
        ui = new DebugUI("Debug Window", game.menuSkin.getDfltSkin());

        ui.setSize(300, 400);
        ui.setPosition(100, 100);
    }

    @Override
    public void preAct(float delta) {
    }

    @Override
    public void postAct(float delta) {
    }

    @Override
    public void preDraw() {
    }

    @Override
    public void postDraw() {
        //        if (drawGrid) drawGrid();

        if (debugB2D && world != null) {
            shapeRenderer.setProjectionMatrix(game.getCam().combined);
            dbgRenderer.render(world,
                            game.getScreen().getLevel().getB2DCam().combined);
            drawBodyCenters();
            //            drawGrid();
        }

        mouseDisplay.set(Gdx.input.getX(), Gdx.input.getY());

        if (drawInfo) {
            batch.begin();
            dbgFont.setScale(1f);
            dbgFont.setColor(Color.WHITE);
            dbgFont.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 0, 20);

            dbgFont.draw(batch, "frame: " + game.getScreen().getFrameNr(), 60,
                            20);

            int x = (int) mouseScreen.x;
            int y = (int) mouseScreen.y;
            dbgFont.draw(batch, "mouse(" + x + ", " + y + ")", 150, 20);

            float xf = (int) (game.getTimeUpdate() * 100f) / 100f;
            dbgFont.draw(batch, "update: " + xf + "ms", 270, 20);

            xf = (int) (game.getTimeRender() * 100f) / 100f;
            xf = System.currentTimeMillis() - xf;
            dbgFont.draw(batch, "render: " + xf + "ms", 380, 20);

            xf = (int) (game.getTimePassed() * 100f) / 100f;
            dbgFont.draw(batch, "passed: " + xf + "ms", 550, 20);

            xf = (int) (Gdx.graphics.getDeltaTime() * 1000f) / 1000f;
            dbgFont.draw(batch, "delta: " + xf + "ms", 660, 20);

            long id = Gdx.graphics.getFrameId();
            dbgFont.draw(batch, "frame: " + id, 770, 20);

            //            dbgFont.draw(batch,
            //                            Gdx.graphics.getWidth() + ", "
            //                                            + Gdx.graphics.getHeight(),
            //                            400, 20);
            //            dbgFont.draw(batch, game.targetRes.x + ", " + game.targetRes.y, 500,
            //                            20);
            batch.end();
        }
    }

    boolean dbgActors = false;

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Keys.F) {
            setDebug(!debugB2D, !drawInfo);
            game.getScreen().setDebug(dbgActors = !dbgActors);
            //            if (drawInfo)
            //                addActor(ui);
            //            else ui.remove();
            return true;
        }

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        game.resume();
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Vector3 v = new Vector3(screenX, screenY, 0);
        //        getCamera().unproject(v);
        mouseScreen.set(v.x, v.y);
        mouseScreen.set(v.x, game.targetRes.y - v.y);
        //        game.getScreen().getLevel().stageToScreenCoordinates(mouseScreen);

        //        stageToScreenCoordinates(mouseScreen);
        //        mouseScreen.x = (int) mouseScreen.x;
        //        mouseScreen.y = (int) mouseScreen.y;
        return false;
    }

    public void setDrawGrid(boolean drawGrid) {
        this.drawGrid = drawGrid;
    }

    public void setDebug(boolean debugB2D, boolean drawInfo) {
        this.debugB2D = debugB2D;
        this.drawInfo = drawInfo;
    }

    public void setDebugB2D(boolean debugB2D) {
        this.debugB2D = debugB2D;
    }

    public void setDrawInfo(boolean drawInfo) {
        this.drawInfo = drawInfo;
    }

    public void setB2DWorld(World world) {
        this.world = world;
    }

    private void drawGrid() {
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.begin(ShapeType.Line);
        for (int x = 0; x < game.res.x; x += 128) {
            shapeRenderer.line(x, 0, x, game.res.y);
        }

        for (int y = 0; y < game.res.y; y += 128) {
            shapeRenderer.line(0, y, game.res.x, y);
        }
        shapeRenderer.end();
    }

    private void drawBodyCenters() {
        Array<Body> bodies = new Array<Body>();
        world.getBodies(bodies);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.begin(ShapeType.Filled);
        batch.begin();
        dbgFont.setScale(.85f);
        Vector2 pos = new Vector2();
        for (Body body : bodies) {
            shapeRenderer.circle(body.getPosition().x * 100, body.getPosition().y * 100, 2.5f);

            pos.set(body.getPosition()).scl(100);
            pos.set(Util.round(pos.x, 2), Util.round(pos.y, 2));

            //            dbgFont.draw(batch, pos.x + ", " + pos.y, pos.x, pos.y);

        }
        batch.end();
        shapeRenderer.end();
    }

    public static void printMapObject(MapObject mo) {
        String key;
        Object value;

        System.out.println(mo.getName());

        MapProperties props = mo.getProperties();
        Iterator<String> iter = props.getKeys();
        while (iter.hasNext()) {
            key = iter.next();
            value = props.get(key);
            System.out.println(key + ": " + value + "[" + value.getClass() + "]");
        }
        System.out.println();
    }

    public static void printMapTile(TiledMapTile t) {
        String key;
        Object value;

        System.out.println("MapTile " + t.getId());
        System.out.println("Offset: " + t.getOffsetX() + ", " + t.getOffsetY());

        MapProperties props = t.getProperties();
        Iterator<String> iter = props.getKeys();
        while (iter.hasNext()) {
            key = iter.next();
            value = props.get(key);
            System.out.println(
                            key + ": " + value + "[" + value.getClass() + "]");
        }
        System.out.println();
    }

    public static void printAtlasRegionInfo(AtlasRegion ar) {
        System.out.println("AtlasRegion " + ar.name + " - " + ar.index);
        System.out.println("Position: " + ar.getRegionX() + ", " + ar.getRegionY());
        System.out.println("Offset  : " + ar.offsetX + ", " + ar.offsetY);
        System.out.println("Size    : " + ar.getRegionWidth() + ", " + ar.getRegionHeight());
        System.out.println("OrigSize: " + ar.originalWidth + ", " + ar.originalHeight);
        System.out.println("Rotate  : " + ar.rotate);
    }
}