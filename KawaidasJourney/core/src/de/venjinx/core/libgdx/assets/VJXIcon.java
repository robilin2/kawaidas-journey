package de.venjinx.core.libgdx.assets;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.kawaida.ui.widgets.KJLabel;

public class VJXIcon extends Stack {

    private Actor actor;
    private Image icon;
    private KJLabel count;

    public VJXIcon(String text, Drawable image, Skin skin, Actor actor) {
        if (actor != null)
        add(actor);

        if (image != null) {
            (icon = new Image(image)).setTouchable(Touchable.disabled);
            add(icon);
        }

        count = new KJLabel(text, skin);
        add(count);

        setTouchable(Touchable.enabled);
    }

    public void setAmount(int amount) {
        count.setText("" + amount + " ");
    }

}
