package de.venjinx.core.libgdx.assets;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class AvatarIcon {

    private Drawable tex;

    public AvatarIcon(Drawable drawable) {
        tex = drawable;
    }

    public Drawable getDrawable() {
        return tex;
    }
}