package de.venjinx.core.libgdx.audio;

import java.util.HashSet;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

public class SoundFX implements Disposable {

    private String name;
    private Sound sound;

    private HashSet<Long> ids;

    protected SoundFX(String name, Sound s) {
        ids = new HashSet<>();
        this.name = name;
        sound = s;
    }

    protected long play() {
        return play(1, 1, 0);
    }

    protected long play(float vol, float pitch, float pan) {
        return sound.play(vol, pitch, pan);
    }

    protected long loop() {
        return loop(1, 1, 0);
    }

    protected long loop(float vol, float pitch, float pan) {
        long id = sound.loop(vol, pitch, pan);
        ids.add(id);
        return id;
    }

    protected void stop() {
        sound.stop();
        ids.clear();
    }

    protected void stop(long id) {
        sound.stop(id);
        ids.remove(id);
    }

    public String getName() {
        return name;
    }

    @Override
    public void dispose() {
        sound.dispose();
        sound = null;
        ids.clear();
        ids = null;
        name = null;
    }
}
