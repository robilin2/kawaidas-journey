package de.venjinx.core.libgdx.audio;

import java.util.HashMap;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import de.venjinx.core.libgdx.VJXAssetManager;

public class AudioController implements Disposable {

    private HashMap<String, SoundFX> sounds = new HashMap<>();

    public void fetchSounds(VJXAssetManager assetMngr) {
        SoundFX sfx;

        Array<Sound> tmpSounds = assetMngr.getAll(Sound.class,
                        new Array<Sound>());

        String name, fileName;
        for (Sound s : tmpSounds) {
            fileName = assetMngr.getAssetFileName(s);
            String[] sa = fileName.split("/");
            name = sa[sa.length - 1].split("\\.")[0];

            sfx = new SoundFX(name, s);

            if (!sounds.containsKey(name))
                sounds.put(name, sfx);
            else
                if (sfx != sounds.get(name)) {
                    sounds.get(name).dispose();
                System.out.println("sound " + name);
                    sounds.put(name, sfx);
                }
        }
    }

    public long play(String name, boolean loop) {
        return playSound(name, loop, 1, 1, 0);
    }

    public long playSound(String name, boolean loop,
                    float volume, float pitch, float pan) {
        SoundFX sfx = sounds.get(name);

        long id;
        if (loop)
            id = sfx.loop(volume, pitch, pan);
        else id = sfx.play(volume, pitch, pan);

        return id;
    }

    public void stopSound(String name) {
        if (!sounds.containsKey(name)) return;

        sounds.get(name).stop();
    }

    public void stopSound(String name, long id) {
        if (!sounds.containsKey(name)) return;

        sounds.get(name).stop(id);
    }

    public void stopAllSounds() {
        for (SoundFX sfx : sounds.values())
            sfx.stop();
    }

    @Override
    public void dispose() {
        for (SoundFX s : sounds.values())
            s.dispose();

        sounds.clear();
        sounds = null;
    }
}
