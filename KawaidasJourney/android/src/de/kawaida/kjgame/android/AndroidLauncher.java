package de.kawaida.kjgame.android;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import android.os.Bundle;
import de.venjinx.core.libgdx.VJXGame;

public class AndroidLauncher extends AndroidApplication {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

        config.useAccelerometer = false;
        config.useCompass = false;

        String[] arg = new String[1];
        arg[0] = "development/level/"
                        + "dev_lvl_00_test_level.tmx";

        VJXGame game = new VJXGame(arg);
        initialize(game, config);
    }
}